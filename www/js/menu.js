window.appRootDirName = "repository";
window.toDownload = new Array();
window.destinationPage="";

document.addEventListener("deviceready", onDeviceReady, false);



$(document).ready(function()
{
	//dovrebbe eseguirlo solo su mamp, non nel device, comunque....

	return;

	console.log("documentREADY");


	var larghezza=$( window ).width();
	var altezza=$( window ).height();
	  
	console.log( "larghezza:" + larghezza );
	console.log( "altezza:" + altezza );

	$( "#main" ).css("height",altezza+"px");	

	var altezzaHeader=altezza*0.05;
	if (altezza>larghezza) altezzaHeader=altezza*0.03;
	$("#header").height(altezzaHeader+"px");

	$( "#background" ).attr("src","img/bkg-home.png");



});

$( window ).resize(function() {
	var larghezza=$( window ).width();
	var altezza=$( window ).height();
	  
	console.log( "larghezza:" + larghezza );
	console.log( "altezza:" + altezza );

	var altezzaHeader=altezza*0.05;
	if (altezza>larghezza) altezzaHeader=altezza*0.03;

	$( "#main" ).css("height",altezza+"px");	

	$("#header").css("height",altezzaHeader+"px");

	var altezzaHeader = $("#header").height();
	console.log( "altezza Header:" + altezzaHeader );

});

function checkConnection()
{
    var networkState = navigator.connection.type;

    var states = {};
    states[Connection.UNKNOWN]  = 'Unknown connection';
    states[Connection.ETHERNET] = 'Ethernet connection';
    states[Connection.WIFI]     = 'WiFi connection';
    states[Connection.CELL_2G]  = 'Cell 2G connection';
    states[Connection.CELL_3G]  = 'Cell 3G connection';
    states[Connection.CELL_4G]  = 'Cell 4G connection';
    states[Connection.NONE]     = 'No network connection';

    //alert('[Connection](connection.html) type: ' + states[networkState]);

    if ((states[networkState]==states[Connection.NONE]) || (states[networkState]==states[Connection.UNKNOWN])) return false;
    else return true;
}	

function onDeviceReady() {
 	//alert("device ready");

	//quento è grande la finestra?
	console.log("window.height: "+$(window).height());   // returns height of browser viewport
	console.log("document.height: "+$(document).height()); // returns height of HTML document
	console.log("window.width: "+$(window).width());   // returns width of browser viewport
	console.log("document.width: "+$(document).width()); 
	

    console.log("deviceREADY");

	var larghezza=$( window ).width();
	var altezza=$( window ).height();
	
	var altezzaHeader=altezza*0.05;
	if (altezza>larghezza) altezzaHeader=altezza*0.03;

	$( "#main" ).css("height",altezza+"px");	
	$("#header").css("height",altezzaHeader+"px");
	console.log("Altezza Header: " +  altezzaHeader);

    var storage = window.sessionStorage;
    window.isApiRest = storage.getItem("isApiRest");

    console.log("window.isApiRest: " + window.isApiRest);

	//per debug....
	//myvar="42/42.html";

	//prendo per buono il json attuale, se ci sono cambiamenti lo rileverà nella home
    window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, gotFS, fail);
    console.log("fine");

}

function getURLParameter(name) {
  return decodeURIComponent((new RegExp('[?|&]' + name + '=' + '([^&;]+?)(&|#|;|$)').exec(location.search)||[,""])[1].replace(/\+/g, '%20'))||null
}


function gotFS(fileSystem) {
    window.fileSystem=fileSystem;
    var dataDir = fileSystem.root.getDirectory(window.appRootDirName, {create: true}, dirReady, failSecond);
}

function fail() {
	console.log("failed to get filesystem");
}

function failSecond() {
	console.log("failed to create folder");
}

function dirReady(entry) {
	console.log("dirReady");
	
	console.log(JSON.stringify(entry));

	//vedo se ci sta il file di definizione e la sua versione
	var filename = window.fileSystem.root.toURL() + window.appRootDirName + "/definizione.json";
	var msg="Live Engine App v1.0";
	var jqxhr = $.getJSON( filename, function(data) {
		console.log( "success" );
		window.definizione=data;
		console.log("definizione caricata: " + JSON.stringify(window.definizione))

	})
	.done(function() {
		console.log( "second success" );
	})
	.fail(function() {
		console.log( "error" );
		msg+="Nessun file di definizione trovato - Si prega di procedere con l'aggiornamento.";
		window.definizione="";
		//ritorno alla home
		window.location.replace("index.html");
	})
	.always(function() {
		console.log( "complete" );
		$("#footerMsg").html(msg);
		aggiustaLayout();

	});	


}

function aggiustaLayout()
{
	var localPath = window.fileSystem.root.toURL() + window.appRootDirName;
	console.log("localPath: " + localPath);

	//menu
	//aggiungo le voci di menu
    var html="";
	for (i=0;i<window.definizione[0].menu.length;i++)
	{
		console.log("menu: " + i);
		//per ogni target vedo gli elementi
	    html+="<div style='margin-bottom: 10%;text-align:left;' onclick='goToMenu("+i+")'>";
	    html+="    <hr class='hr' style='width:80%;margin-bottom:10%;'>";
	    html+="    <span class='menuText' style='color:#000000;margin-top:0%;margin-bottom:0%;margin-left:10%;'>"+window.definizione[0].menu[i].nome+"</span>";
	    html+="</div>";
	}
	$("#menuDiv").append(html);

	//colori
	$( "#titoloApp" ).css("color",window.definizione[0].layout.coloreTitolo);
	$( "#footerMsg" ).css("background-color",window.definizione[0].layout.coloreGenerale);
	$( "#footerMsg" ).css("color",window.definizione[0].layout.coloreTesto);
	$( ".fa-bars" ).css("color",window.definizione[0].layout.coloreGenerale);
	$( "#menuDiv" ).css("background-color",window.definizione[0].layout.coloreSfondoMenu);
	$( ".menuText" ).css("color",window.definizione[0].layout.coloreVoceMenu);
	$( ".hr" ).css("border-color",window.definizione[0].layout.coloreVoceMenu);

	
	//immagine del menu
	var myvar = getURLParameter('indice');
	var nomeImmagine=window.definizione[0].menu[myvar].path;
	console.log("window.isApiRest: "+window.isApiRest);
	if (window.isApiRest=="true")
	{
		console.log("dentro api rest");
		var newPath = decodeURIComponent(nomeImmagine);
		var n = newPath.lastIndexOf(".");
		var estensione=newPath.substring(n+1);
		nomeImmagine="menu"+myvar+"."+estensione;
	}
	else
	{
		console.log("fuori api rest");
		if (nomeImmagine.startsWith("menuCliente")) nomeImmagine=nomeImmagine.substring(12);
		else nomeImmagine=nomeImmagine.substring(5);
	}
	console.log("Immagine del menu: " + nomeImmagine);
	$( "#background" ).attr("src",localPath+"/"+nomeImmagine);
	//$( "#immagine" ).attr("src",localPath+"/"+nomeImmagine);

	$("#footerMsg").html(window.definizione[0].layout.nome + " AR App v1.0");


}





