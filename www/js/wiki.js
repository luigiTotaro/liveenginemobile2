function unzippaFileAndStart(zipFile, url, config)
{
	console.log("sto unzippando "+ zipFile);
	
	zip.unzip(zipFile, url,
		function(status) {
			console.log("finito di unzippare con codice: " + status);
			wikitudePlugin.close();
			var value=window.fileSystem.root.toURL() + window.appRootDirName + "/contenuti/" + JSON.parse(config).call;
			console.log("devo chiamare la pagina: "+ value);
			document.location = "iframe.html?url="+value;
			
	    	

	    },
		function(progressEvent) {
    		//console.log("unzip: " + Math.round((progressEvent.loaded / progressEvent.total) * 100)+"%");
    		//console.log("unzip raw: " + progressEvent.loaded + " - " + progressEvent.total);
		});
}



function loadARchitectWorld (example)
{
    console.log("Entrato in loadARchitectWorld");
    // check if the current device is able to launch ARchitect Worlds
    wikitudePlugin.isDeviceSupported(function()
    {
     	console.log("Device is supported");
       //app.wikitudePlugin.setOnUrlInvokeCallback(app.onUrlInvoke);
        

        wikitudePlugin.loadARchitectWorld(function successFn(loadedURL)
        {
            //console.log("loadARworld ok, call calljavascript: "+example.localPath);
            /* Respond to successful world loading if you need to */ 
            
            console.log("dovrei averla chiamata....");

        }, function errorFn(error)
        {
            console.log("Loading AR web view failed");
            alert('Loading AR web view failed: ' + error);
        },
        example.path, example.requiredFeatures, example.startupConfiguration
        );
        wikitudePlugin.callJavaScript('passData("'+example.localPath+'","'+example.idPrj+'",'+example.isApiRest+',"'+example.baseUrl+'","'+example.deviceId+'")');
        wikitudePlugin.setOnUrlInvokeCallback(function (url)
	        {
		        console.log("url chiamata: "+ url);

				var parsedUrl= encodeURI(url.substring(15));
		        
		        var check=parsedUrl.substring(0,7);
		        
		        if (check=="storage")
		        {
		        	var value=parsedUrl.substring(7);
				    console.log("Memorizzo nella sessionStorage il valore: "+ value);
				    var storage = window.sessionStorage;

				    storage.setItem("fase",value);

		        }
		        else
		        {
			        
			        var check2=parsedUrl.substring(0,14);

			        if (check2=="scaricaHtmlZip")
			        {
			        	var value=parsedUrl.substring(15);
						value=decodeURIComponent(value);

					    console.log("json config: "+ value);

						config=decodeURIComponent(value);

					    console.log("decodificato: "+ config);

					    var url=JSON.parse(config).path;
						
						var name="";
						var dir="";

						var pathSplit= url.split("/");
						name=pathSplit[pathSplit.length-1];
						dir=pathSplit[pathSplit.length-2];

						var path = window.baseUrl + "/" + url;

						var uri = encodeURI(path);
						
						//potrei averlo già scaricato..... vedo la dimensione del file remoto
						console.log("check dimensione file remoto: " + path);
						$.ajax({
							type: "HEAD",
							async: true,
							url: uri,
						}).done(function(message,text,jqXHR){
							var dimensioneRemota=jqXHR.getResponseHeader('Content-Length');
							console.log("la dimensione del file remoto è di " + dimensioneRemota);
							//console.log(jqXHR.getResponseHeader('Content-Length'));
							
							//ora vedo se esiste e quale dimensione ha il file locale 
							window.fileSystem.root.getDirectory(window.appRootDirName + "/contenuti", {create: false}, function(entry) {
									console.log("-- GETDIR -- got the dir" + JSON.stringify(entry));

									entry.getFile(name, {create: false, exclusive: false}, function(file) {
											console.log("-- GETFILE -- got the file" + JSON.stringify(file));
											file.getMetadata(
						                                function (metadata) {
						                                    console.log(JSON.stringify(metadata)); // get file size
						                                    console.log(metadata.size); // get file size
						                                    if (dimensioneRemota==metadata.size)
						                                    {
						                                    	//NON scarico il file
																wikitudePlugin.close();
																var value=window.fileSystem.root.toURL() + window.appRootDirName + "/contenuti/" + JSON.parse(config).call;
																console.log("devo chiamare la pagina: "+ value);
																document.location = "iframe.html?url="+value;

						                                    }
						                                    else
						                                    {
						                                    	//dimensione diversa, scarico il file
						                                    	scaricaEunzippa (uri, dir, name, config);
						                                    }

						                                }, 
						                                function (error) {
						                                	console.log("Errore recuperando i metadati");
						                                	//erorre, scarico il file
						                                	scaricaEunzippa (uri, dir, name, config)
						                                }
						                            );
																		
										}, function(){
									        console.log("-- GETFILE -- errore durante il get del file");
									        //non esiste il file, lo scarico
									        scaricaEunzippa (uri, dir, name, config)
										});	


																
								}, function(){
									        console.log("-- GETDIR -- errore durante il get della dir");
											//la dir non esiste, a maggior ragione il file... lo scarico
											scaricaEunzippa (uri, dir, name, config)	
									})	





						}); 



/*

						var lastPerc=-1;

						ft = new FileTransfer();
						ft.onprogress = function(progressEvent) {
						    if (progressEvent.lengthComputable) {
						    	//console.log("download status: "+  progressEvent.loaded + " di " + progressEvent.total);
						    	var perc=Math.round((progressEvent.loaded / progressEvent.total)*100);
								if (perc!=lastPerc)
								{
									lastPerc=perc;
									//console.log(perc);
									wikitudePlugin.callJavaScript('downloadProgress('+perc+')');
								}
								//drawProgress(document.getElementById('activeProgress2'), perc/100, $('.progress-bar2 p'));
						    }
						    else
						    {
						    	console.log("download status: Increment");
						    }
						};

						ft.download(
						    uri,
						    window.fileSystem.root.toURL() + window.appRootDirName + "/" + dir + "/" + name,
						    //window.appRootDir.fullPath + "/" + fileName,
						    function(entry) {
						        console.log("download complete: " + entry.fullPath);
					    		wikitudePlugin.callJavaScript('downloadMessage("Attendere, configurazione del contenuto in corso")');
					    		unzippaFileAndStart(window.fileSystem.root.toURL() + window.appRootDirName + "/" + dir + "/" + name,window.fileSystem.root.toURL() + window.appRootDirName + "/" + dir,config);
						    },

						    function(error) {
						        console.log("download error source: " + error.source);
						        console.log("download error target: " + error.target);
						        console.log("upload error code: " + error.code);
						        console.log(JSON.stringify(error));
						        //code: 1: non ha trovato il file
								alert("Attenzione. Errore nello scaricamento del contenuto. Contattare assistenza. Codice errore: " + error.code);		

						    }
						);
*/


			        }
			        else
					{
				        // TODO: impl. url parsing to know what to do with the given url.
				        //alert(url);
				        wikitudePlugin.close();
				        //estraggo dove devo andare
				        //var parsedUrl= encodeURI(url.substring(15));
				        
				        //alert(parsedUrl);
				        
				        
				        //document.location = "iframe.html?url="+parsedUrl;
				        


						//parsedUrl = encodeURI("file:///data/data/com.mediasoft.ar/files/files/repository/pr_020/page.html");
				        //document.location = "iframe.html?url="+parsedUrl;
				        
				        //document.location = "file:///data/data/com.mediasoft.ar/files/files/repository/pr_020/page.html";
								       
				        document.location = "index.html";

				        //document.location = parsedUrl;
					}
	    		}
	    	});

    }, function(errorMessage)
    {
    	console.log("Device is NOT supported");
        alert(errorMessage);
    },
    example.requiredFeatures
    );
}

function scaricaEunzippa (uri, dir, name, config)
{
	console.log("Scarico e Unzippo - " + uri + " - " + dir + " - " + name + " - " + config);
	wikitudePlugin.callJavaScript('startDownloading()');

	var lastPerc=-1;

	ft = new FileTransfer();
	ft.onprogress = function(progressEvent) {
	    if (progressEvent.lengthComputable) {
	    	//console.log("download status: "+  progressEvent.loaded + " di " + progressEvent.total);
	    	var perc=Math.round((progressEvent.loaded / progressEvent.total)*100);
			if (perc!=lastPerc)
			{
				lastPerc=perc;
				//console.log(perc);
				wikitudePlugin.callJavaScript('downloadProgress('+perc+')');
			}
			//drawProgress(document.getElementById('activeProgress2'), perc/100, $('.progress-bar2 p'));
	    }
	    else
	    {
	    	console.log("download status: Increment");
	    }
	};

	ft.download(
	    uri,
	    window.fileSystem.root.toURL() + window.appRootDirName + "/" + dir + "/" + name,
	    //window.appRootDir.fullPath + "/" + fileName,
	    function(entry) {
	        console.log("download complete: " + entry.fullPath);
    		wikitudePlugin.callJavaScript('downloadMessage("Attendere, configurazione del contenuto in corso")');
    		unzippaFileAndStart(window.fileSystem.root.toURL() + window.appRootDirName + "/" + dir + "/" + name,window.fileSystem.root.toURL() + window.appRootDirName + "/" + dir,config);
	    },

	    function(error) {
	        console.log("download error source: " + error.source);
	        console.log("download error target: " + error.target);
	        console.log("upload error code: " + error.code);
	        console.log(JSON.stringify(error));
	        //code: 1: non ha trovato il file
			alert("Attenzione. Errore nello scaricamento del contenuto. Contattare assistenza. Codice errore: " + error.code);		

	    }
	);


}