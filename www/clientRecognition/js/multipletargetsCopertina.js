var World = {
	loaded: false,
	rotating: false,


	init: function initFn() {
		this.loaded=false;
		//AR.context.destroyAll();
		this.createOverlays();
	},

	createOverlays: function createOverlaysFn() {
		/*
			First an AR.ClientTracker needs to be created in order to start the recognition engine. It is initialized with a URL specific to the target collection. Optional parameters are passed as object in the last argument. In this case a callback function for the onLoaded trigger is set. Once the tracker is fully loaded the function worldLoaded() is called.

			Important: If you replace the tracker file with your own, make sure to change the target name accordingly.
			Use a specific target name to respond only to a certain target or use a wildcard to respond to any or a certain group of targets.

			Adding multiple targets to a target collection is straightforward. Simply follow our Target Management Tool documentation. Each target in the target collection is identified by its target name. By using this target name, it is possible to create an AR.Trackable2DObject for every target in the target collection.
		*/



		//alert("assets/"+oggetto.tracker);

		this.tracker = new AR.ClientTracker(contenutiPath + wtcName, {
			onLoaded: this.worldLoaded
		});
		/*
		this.tracker = new AR.ClientTracker("assets/tracker.wtc", {
			onLoaded: this.worldLoaded
		});
		*/
		
		//this.rotationAnimation = new AR.PropertyAnimation(this.modelCar, "rotate.roll", -25, 335, 10000);

		arrayOverlay = [];

		var trackable = new AR.Trackable2DObject(this.tracker, "*", {
			drawables: {
				cam: arrayOverlay
			},
			onEnterFieldOfVision: function onEnterFieldOfVisionFn(a)
            {
				console.log("ho rilevato " + a);
				//console.log("aspectRatio: "+trackable.aspectRatio);
				var nome="";

				//var cssDivInstructions = " style='display: table-cell;vertical-align: middle; text-align: center; width: 50%; padding-right: 15px;'";
				//document.getElementById('loadingMessage').innerHTML ="<div" + cssDivInstructions + ">" + a + "</div>";

				document.location = 'architectsdk://copertina'+a;

				trackable.snapToScreen.enabled = false;
			},
			snapToScreen: {
				enabledOnExitFieldOfVision: false,
				snapContainer: document.getElementById('snapContainer')
			},	
			onExitFieldOfVision: function onExitFieldOfVisionFn(a)
            {
				console.log("ho perso la rilevazione di " + a);
				//var cssDivInstructions = " style='display: table-cell;vertical-align: middle; text-align: center; width: 50%; padding-right: 15px;'";
				//document.getElementById('loadingMessage').innerHTML ="<div" + cssDivInstructions + ">Looking for Target.....</div>";

				trackable.snapToScreen.enabled = false;
			},
		});


	},


	toggleAnimateModel: function toggleAnimateModelFn()
	{
		if (!World.rotationAnimation.isRunning()) {
			if (!World.rotating) {
				// Starting an animation with .start(-1) will loop it indefinitely.
				World.rotationAnimation.start(-1);
				World.rotating = true;
			} else {
				// Resumes the rotation animation
				World.rotationAnimation.resume();
			}
		} else {
			// Pauses the rotation animation
			World.rotationAnimation.pause();
		}

		return false;
	},



	showContents: function (item)
	{
        return function() {
			//alert(url);
			console.log("Sono nello showContents");
			//showImage (parameter + "/" + url);
			showContents (item);
        };
	},

	mostraContenuti: function (indiceImmagine, indiceElemento)
	{
        return function() {
			console.log("sono in mostraContenuti, con indiceImmagine: "+indiceImmagine + " e indiceElemento: " + indiceElemento);
			
			console.log("dati: " +JSON.stringify(oggetto.target[indiceImmagine].punti[indiceElemento]));

			showContenuto(indiceImmagine, indiceElemento);
        };
	},	



	worldLoaded: function worldLoadedFn() {
		console.log("worldLoaded");

		setTimeout(function() {
			//var e = document.getElementById('loadingMessage');
			//e.parentElement.removeChild(e);
		
			/*
			$("#myPano").pano({
				img: "./panorama/sphere.jpg"
			});
*/
		}, 1000);
	}



};


//var parameter = "file:///var/mobile/Containers/Data/Application/FDB98D9D-BFC3-4497-A720-81E52BA126AB/Documents/repository";

var logEnabled= false;

if (logEnabled==true)
{
	AR.logger.activateDebugMode();
}

console.log("sono qui dentro");







function passData(localPath, wtc)
{   
	console.log("v 1.0 - passData: "+localPath + " - " + wtc);
	//console.log("v 1.0 - passData: "+localPath + " - " + wtc);

	contenutiPath = localPath + "/";
	wtcName=wtc;


	//quento è grande la finestra?
	console.log("window.height: "+$(window).height());   // returns height of browser viewport
	console.log("document.height: "+$(document).height()); // returns height of HTML document
	console.log("window.width: "+$(window).width());   // returns width of browser viewport
	console.log("document.width: "+$(document).width()); 

	if (logEnabled==true) AR.logger.info("v 1.0 - passData: "+localPath);

	var larghezza=$( window ).width();
	var altezza=$( window ).height();
	var altezzaHeader=altezza*0.05;


	if (larghezza>altezza)
	{
		//landscape
		$("#messaggioIniziale").css("width","50%").css("height",altezza/3+"px").css("margin-left","25%").css("margin-top",((altezza-(altezza/3))/2)+"px");
		//$("#messaggioIniziale p").css("line-height",altezza/3+"px");
		$("#logoAR").css("width","24%")
	}
	else
	{
		$("#messaggioIniziale").css("width","80%").css("height",altezza/4+"px").css("margin-left","10%").css("margin-top",((altezza-(altezza/4))/2)+"px");
		//$("#messaggioIniziale p").css("line-height",altezza/4+"px");
		$("#logoAR").css("width","33%")
	}
	
	$("#messaggioIniziale").show();
	$("#bottoneRitorna").show();





	setTimeout(function() {
		$("#bottoneNonRiesco").show();
	}, 10000);

	setTimeout(function() {
		$("#messaggioIniziale").hide();
	}, 5000);

	loadJson(localPath+"/labels.json");
	//World.init();

}

var baseUrl = "http://192.170.5.25:8888/liveEngine_webapp";
var contenutiPath="";
var idProgetto="";
var isApiRest = false;
var immaginiVotate= new Array(); //memorizzo le immagini per cui ha già votato, in modo da non farlo votare più (almeno fini a quando non esce e rientra dalla RA)

var itemRilevato="";
var linguaScelta="it";
var livelloAttuale=1;
var livelloPadre="";

var	oggetto = new Array();
var	oggettoTemp = new Array();
var	arrayOverlay = new Array();
var	arrayOverlayGlobale = new Array(); //contiene n arrayOverlay, uno per ogni immagine da riconoscere nel tracker....
var	arrayIndiciIndicatori = new Array(); //contiene gli indici degli indicatori principali nel'arrayOverlay
var	arrayIndiciIndicatoriGlobale = new Array(); //contiene n arrayIndiciIndicatori, uno per ogni immagine da riconoscere nel tracker....
var	indiceIndicatoreCorrente = -1;
var	arrayCorrispondenzeNomi = new Array(); //contiene n nomi, che mi indicano a quale oggetto dell'array globale mi riferisco
var	arrayTrackable = new Array();
var rotationAnimation;
var arrayAnim = new Array();

var arrayIndiciOverlay_IndiciOggetto = new Array(); //questo array mi mette in relazione l'indice dell'oggetto overlay con l'ondice in oggetto (la variabile)

var videoIsPlaying=false;
var actualVideo=null;
var labelsObj;


var inquadratura = 0; //0=tutta la caldaia - 1=superiore - 2=inferiore

//window.appRootDirName = "repository";
//window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, gotFS, fail);

//$( window ).click(function(){
//  alert($(this).attr('id'));
//});


$( window ).resize(function() {
	var larghezza=$( window ).width();
	var altezza=$( window ).height();
	  
	console.log( "larghezza:" + larghezza );
	console.log( "altezza:" + altezza );

	if (larghezza>altezza)
	{
		//landscape
		$("#messaggioIniziale").css("width","50%").css("height",altezza/3+"px").css("margin-left","25%").css("margin-top",((altezza-(altezza/3))/2)+"px");
		//$("#messaggioIniziale p").css("line-height",altezza/3+"px");
		$("#logoAR").css("width","27%")
	}
	else
	{
		$("#messaggioIniziale").css("width","80%").css("height",altezza/4+"px").css("margin-left","10%").css("margin-top",((altezza-(altezza/4))/2)+"px");
		//$("#messaggioIniziale p").css("line-height",altezza/4+"px");
		$("#logoAR").css("width","33%")
	}

  

});


function loadJson(myUrl)
{

	oggetto = new Object();
	console.log("carico il file: " + myUrl);

	if (logEnabled==true) AR.logger.info("url: " + myUrl)

	$.ajax({
		url: myUrl,
		dataType: 'json'
		})
		.done(function( json )
		{
			console.log("file json caricato");
			labelsObj=json;
			
			//traduco le labels
			$("#bottoneRitorna").html(getLabel("BTN_RITORNA","it","Ritorna"));
			$("#bottoneNonRiesco").html(getLabel("BTN_NON_RILEVO_COPERTINA","it","Non riesco a rilevare la copertina"));
			$("#testoInquadraCopertina").html(getLabel("INQUADRA_COPERTINA","it","Inquadra la copertina della tua rivista"));


			World.init();


			
		})
		.fail(function( jqXHR, textStatus ) {
	  		if (logEnabled==true) AR.logger.info("Request failed: " + textStatus)
	  		console.log( "Request failed: " + textStatus );

		}); 
}




function scegliOverlay(itemRilevato)
{
	if (logEnabled==true) AR.logger.info("scegliOverlay: "+ itemRilevato);
	console.log("sono in scegliOverlay, con nome: "+itemRilevato);
    
	//nome da far comparire in alto
	var nome="";
	var gradimento;
	var idImmagine;
	for (i=0;i<oggetto.target.length;i++)
	{
		if (oggetto.target[i].targetName==itemRilevato)
		{
			nome=oggetto.target[i].imageName;
			gradimento=oggetto.target[i].gradimento;
			idImmagine=oggetto.target[i].imageId;
		}
	}	
	var cssDivInstructions = " style='display: table-cell;vertical-align: middle; text-align: center; width: 50%; padding-right: 15px;'";
	document.getElementById('loadingMessage').innerHTML ="<div" + cssDivInstructions + ">" + nome + "</div>";


	var time=0;
	//abilito a seconda dell'oggetto
	for (i=0;i<arrayOverlay.length;i++)
	{
		if (arrayOverlay[i].targetRef==itemRilevato)
		{
			abilitaOggetto(arrayOverlay[i],time);
			time = time+300;
		}
		else arrayOverlay[i].enabled=false;
	}

	//devo mettere il gradimento?
	if (gradimento==1)
	{
		//ha già votato per questa immagine?
		console.log("Immagini Votate: " + JSON.stringify(immaginiVotate));
		if (immaginiVotate.indexOf(parseInt(idImmagine)) == -1) //puo' votare
		{
			var idProgetto = oggetto.idProgetto;
			var html="";
			html += "<p class='testoMini' style='width:100%;text-align:center;color:#ffffff;margin-top:-0.5%;margin-bottom: 0.5%;'>Ciao, vuoi dare un giudizio a questo articolo?</p>";    
			var iconeAttive=oggetto.gradimento.iconeAttive;
			console.log("Icone Attive: " + iconeAttive);
			var iconeAttiveArray=iconeAttive.split(",");
			var dimensioneSingolo=100/iconeAttiveArray.length;
			console.log("dimensione div: " + dimensioneSingolo);
			if (iconeAttive.search("1") != -1)
			{
				var pathArray=oggetto.gradimento.icona1.split("/");
				path=pathArray[pathArray.length-1];
				html+='<div style="width:'+dimensioneSingolo+'%;border:0px solid red;float:left;text-align:center;height:100%;color:white;" onClick="vota('+idProgetto+','+idImmagine+','+oggetto.gradimento.valore1+');"><img src="'+ contenutiPath + path + '" style="height:75%"><br>'+oggetto.gradimento.nome1+'</div>';
			}
			if (iconeAttive.search("2") != -1)
			{
				var pathArray=oggetto.gradimento.icona2.split("/");
				path=pathArray[pathArray.length-1];
				html+='<div style="width:'+dimensioneSingolo+'%;border:0px solid red;float:left;text-align:center;height:100%;color:white;" onClick="vota('+idProgetto+','+idImmagine+','+oggetto.gradimento.valore2+');"><img src="'+ contenutiPath + path + '" style="height:75%"><br>'+oggetto.gradimento.nome2+'</div>';
			}
			if (iconeAttive.search("3") != -1)
			{
				var pathArray=oggetto.gradimento.icona3.split("/");
				path=pathArray[pathArray.length-1];
				html+='<div style="width:'+dimensioneSingolo+'%;border:0px solid red;float:left;text-align:center;height:100%;color:white;" onClick="vota('+idProgetto+','+idImmagine+','+oggetto.gradimento.valore3+');"><img src="'+ contenutiPath + path + '" style="height:75%"><br>'+oggetto.gradimento.nome3+'</div>';
			}
			if (iconeAttive.search("4") != -1)
			{
				var pathArray=oggetto.gradimento.icona4.split("/");
				path=pathArray[pathArray.length-1];
				html+='<div style="width:'+dimensioneSingolo+'%;border:0px solid red;float:left;text-align:center;height:100%;color:white;" onClick="vota('+idProgetto+','+idImmagine+','+oggetto.gradimento.valore4+');"><img src="'+ contenutiPath + path + '" style="height:75%"><br>'+oggetto.gradimento.nome4+'</div>';
			}
			if (iconeAttive.search("5") != -1)
			{
				var pathArray=oggetto.gradimento.icona5.split("/");
				path=pathArray[pathArray.length-1];
				html+='<div style="width:'+dimensioneSingolo+'%;border:0px solid red;float:left;text-align:center;height:100%;color:white;" onClick="vota('+idProgetto+','+idImmagine+','+oggetto.gradimento.valore5+');"><img src="'+ contenutiPath + path + '" style="height:75%"><br>'+oggetto.gradimento.nome5+'</div>';
			}
			html+='<div style="clear:both;"></div>';

			$("#gradimentoContainer").html(html);
			$("#gradimentoContainer").show();	

			console.log("html: "+html);	
		}
		else
		{
			console.log("Ha già votato per questa immagine in questa sessione di RA");
		}

	}

}

function vota(idProgetto,idImmagine,voto)
{

	var url=baseUrl + "/votaGradimento.php?idProgetto="+idProgetto+"&idImmagine="+idImmagine+"&voto="+voto;
	console.log(url);

	$.ajax({
	  url: url,
	  type:"GET"
	}).done(function ( data ) {
		console.log(data);
		if (data=="VOTE_SET")
		{
			immaginiVotate.push(idImmagine);
			alert("Grazie, il tuo voto è stato correttamente registrato!");
		}
		else if (data=="CANT_VOTE_FOR_IMAGE")
		{
			alert("Attezione, problema tecnico nel voto");
		}
		else if (data=="TOO_EARLY_FOR_RE-VOTE")
		{
			alert("Attezione, non puoi votare di nuovo per questo articolo");
		}
		$("#gradimentoContainer").hide();	
		
	});

}

function abilitaOggetto(obj,time)
{
	console.log("sono in abilitaOggetto, con time=" + time);
	setTimeout(function() {
		obj.enabled=true
		var animation = new AR.PropertyAnimation(obj, "scaling", 0, 1, 500, 
		 {type: AR.CONST.EASING_CURVE_TYPE.EASE_OUT_CUBIC});

		animation.start(1);

	}, time);


}

function abilitaOverlay(toEnable)
{
	if (logEnabled==true) AR.logger.info("abilita abilitaOverlay: "+ itemRilevato);
	console.log("sono in abilitaOverlay, con nome: "+itemRilevato);
    
	
	if (toEnable) scegliOverlay();
	else
	{
		//abilito a seconda della maniglia rilevata
		for (i=0;i<arrayOverlay.length;i++)
		{
			arrayOverlay[i].enabled=toEnable;
		}
		rotationAnimation.stop();	
	}
}


function showContenuto(immagine, punto)
{
	if (oggetto.target[immagine].punti[punto].tipo==1) //video
	{
 		if (isApiRest)
		{
			//var n = oggetto.target[immagine].punti[punto].path.lastIndexOf(".");
			//var estensione=oggetto.target[immagine].punti[punto].path.substring(n+1);
			var oggettoVideo=JSON.parse(oggetto.target[immagine].punti[punto].config);
			var pathVideo=oggettoVideo.path;
			var n = pathVideo.lastIndexOf(".");
			var estensione=pathVideo.substring(n+1);
			creaVideo("contenuti/" + oggetto.target[immagine].punti[punto].id+"."+estensione);
		}
		else
		{
			var oggettoVideo=JSON.parse(oggetto.target[immagine].punti[punto].config);
			creaVideo("contenuti/" + oggettoVideo.path);
		}

	}
	else if (oggetto.target[immagine].punti[punto].tipo==2) //slideshow
	{
 		var arrayImg=[];
		var arrayTitoli=[];

		//for (i=0;i<oggetto.target[immagine].punti[punto].immagini.length;i++)
		var arrayOggettoImmagine=JSON.parse(oggetto.target[immagine].punti[punto].config)
		for (i=0;i<arrayOggettoImmagine.length;i++)
		{
			var oggettoImmagine=arrayOggettoImmagine[i];
			//console.log("oggettoImmagine: " + oggettoImmagine);
			//console.log("oggettoImmagine (json): " + JSON.stringify(oggettoImmagine));
	 		if (isApiRest)
			{
				var pathImmagine=oggettoImmagine.path;
				var n = pathImmagine.lastIndexOf(".");
				var estensione=pathImmagine.substring(n+1);
				//var n = oggetto.target[immagine].punti[punto].immagini[i].lastIndexOf(".");
				//var estensione=oggetto.target[immagine].punti[punto].immagini[i].substring(n+1);
				arrayImg.push("contenuti/" + oggetto.target[immagine].punti[punto].id+"_"+i+"."+estensione);
			}
			else
			{
				arrayImg.push("contenuti/" + oggettoImmagine.path);
			}
			//else arrayImg.push("contenuti/" + oggetto.target[immagine].punti[punto].immagini[i]);
		}

		creaSlideshow(arrayImg);

	}
	else if (oggetto.target[immagine].punti[punto].tipo==3) //testo
	{
		var oggettoTesto=JSON.parse(oggetto.target[immagine].punti[punto].config)
 		creaTesto(oggettoTesto.text);

	}


}



function creaVideo(urlVideo)
{
		nasconditutto();

		$("#backButton").hide();

		//quanto lo devo fare grande il video?
		var larghezzaDiv=$("#videoContainer").width();


		var htmlToAdd="";
		htmlToAdd += "<video id='videoInterno' autoplay controls width='"+larghezzaDiv*0.95+"' style='padding-top:"+larghezzaDiv*0.025+"px;padding-bottom:"+larghezzaDiv*0.025+"px;'>";
		
		//su alcuni device non riesce a prendere il video da assets. percio' si deve mettere sul device (in downloads per esempio) e puntarlo da li
		htmlToAdd += "<source src='"+ contenutiPath + urlVideo+"' type='video/mp4' />";
		//htmlToAdd += "<source src='assets/"+urlVideo+"' type='video/mp4' />";
		
		htmlToAdd += "</video>";
		htmlToAdd += "<image src='assets/btn_close.png' onclick='back();' style='height:80px;border: 0px;background: none;box-shadow: none;margin: 0px;padding: 0px;right: -10px;top: -2px;position:absolute;' />";

		console.log(htmlToAdd);

		$("#videoContainer").html(htmlToAdd);



		var video = document.getElementById('videoInterno');
		video.addEventListener('loadeddata', function() {
		    console.log("Loaded the video's data!");

		    //calcolo l'altezza del contenitore
		    var altezzaVideo = $("#videoInterno")[0].videoHeight;
		    var larghezzaVideo = $("#videoInterno")[0].videoWidth;
		    var larghezzaContenitore=$("#videoContainer").width();

		    //var altezzaContenitore=$("#videoContainer").height();
		    var altezzaContenitore=altezzaVideo/larghezzaVideo*larghezzaContenitore;



		    //var altezzaContenitore=$("#videoContainer").height();
			var altezzaHeader = $("#header").height();
			var altezzaFooter = $("#footerMsg").height();
			var altezza=$( window ).height();
			var posizione=(altezza-altezzaHeader-altezzaContenitore-altezzaFooter)/2 + altezzaHeader;
			console.log("altezzaContenitore: " + altezzaContenitore);
			console.log("altezzaHeader: " + altezzaHeader);
			console.log("altezzaFooter: " + altezzaFooter);
			console.log("altezza: " + altezza);
			console.log("posizione: " + posizione);

			$("#videoContainer").css("top",posizione+"px");
			$("#videoContainer").show();

		}, false);




}

function creaSlideshow(arrayImg)
{

		nasconditutto();

		$("#backButton").hide();

		//grandezza del carattere...
		var larghezza=$( window )[0].innerWidth;
		var altezza=$( window )[0].innerHeight;
		var altezzaHeader = $("#header").height();
		var altezzaFooter = $("#footerMsg").height();
		

		var altezzaImmagine=(altezza-altezzaHeader-altezzaFooter)*0.7;
		

		//var altezzaImmagine=altezza*0.4;
		//if (altezza<=541) altezzaImmagine=altezza*0.8;
		//else if (altezza<=800) altezzaImmagine=altezza*0.5;

		console.log( "larghezza:" + larghezza );
		console.log( "altezza:" + altezza );
		console.log( "altezzaImmagine:" + altezzaImmagine );
		var charSize="100%";

		if (larghezza>1600) charSize="60%";
		else if (larghezza>1500) charSize="60%";
		else if (larghezza>1400) charSize="60%";
		else if (larghezza>1300) charSize="60%";
		else if (larghezza>1200) charSize="60%";
		else if (larghezza>1100) charSize="60%";
		else if (larghezza>1000) charSize="60%";
		else if (larghezza>900) charSize="60%";
		else if (larghezza>800) charSize="60%";
		else if (larghezza>700) charSize="60%";
		else if (larghezza>600) charSize="60%";
		else charSize="60%";


		var htmlToAdd="";

/*
		// ne metto prima una che illustra lo slide
		if (arrayImg.length>1)
		{
			htmlToAdd += "<section>";
			htmlToAdd += "<div style='background-color:white;text-align:center;width: 80%;border: 1px solid #aaaaaa;border-radius: 20px;position: absolute;right: 10%;top:50px;'>";
			htmlToAdd += "<image src='assets/slide.png' style='width:60%;margin-bottom: 0px;border: 0px;background: none;margin: 0px;margin-left:10%;box-shadow: none;' />";
			htmlToAdd += "<div style='clear:both;width:100%;display: inline-flex;'>";
			//htmlToAdd += "<p class='testoMini' style='width:80%;text-align:center;color:#b1c903;margin-top:0%;padding-top: 2%;margin-bottom: 2%;margin-left:10%;border:0px solid green;font-size:"+charSize+"'>Scorri le slide per esaminare le schede dei prodotti</p>";
			htmlToAdd += "</div>";
			htmlToAdd += "</div>";
			htmlToAdd += "</section>";
		}
*/

			$( "#videoContainer" ).css("border","2px solid " + oggetto.coloreGenerale);


		for (i=0;i<arrayImg.length;i++)
		{
			htmlToAdd += "<section>";
			htmlToAdd += "<div style='background-color:white;text-align:center;width: 80%;border: 2px solid " + oggetto.coloreGenerale + ";border-radius: 20px;position: absolute;right: 10%;top:0px;'>";
			//htmlToAdd += "<p class='menuTitle' style='text-align:center;color:#b1c903;margin-top:3%;margin-bottom: 0px;font-size:80%;'>" + arrayTitoli[i] + "</p>";
			htmlToAdd += "<image src='"+ contenutiPath + arrayImg[i] + "' style='height:"+altezzaImmagine+"px;margin-bottom: 0px;border: 0px;background: none;margin: 0px;box-shadow: none;margin-top: 2%;' />";
			htmlToAdd += "<image src='assets/btn_close.png' onclick='back();' style='height:80px;border: 0px;background: none;box-shadow: none;margin: 0px;padding: 0px;right: -10px;top: -2px;position:absolute;' />";
			//htmlToAdd += "<div style='clear:both;width:100%;display: inline-flex;'>";
			//htmlToAdd += "</div>";
			htmlToAdd += "</div>";
			htmlToAdd += "</section>";


		}
		console.log(htmlToAdd);

		$("#mySlides").html(htmlToAdd);
		$("#slideShow").show();
		

		//$("#backButton").show();

		var altezzaHeader = $("#header").height();
		//var larghezzaImg = $("#imgChiudi").width();
		$("#slideShow").css("top",altezzaHeader);
		//$("#labelChiudi").css("font-size",altezzaHeader*0.6);
		//$("#labelChiudi").css("margin-top",altezzaHeader*0.2);
		//$("#labelChiudi").css("left",larghezzaImg+"px");


	    Reveal.addEventListener( 'ready', function( event ) {
	    	console.log("Reveal Ready");
	    	Reveal.slide( 0 );
	    	Reveal.configure({ transition: 'slide' });
		} );

	    Reveal.initialize({
	        margin: 0.1,
	        //override width
	        height: (altezza-altezzaHeader-altezzaFooter),
	        //height: 550,
	        
	        //override width
	        width: larghezza,
	        //width: 940,
	        // Display controls in the bottom right corner
	        controls: false,
	        // Display a presentation progress bar
	        progress: false,
	        // Display the page number of the current slide
	        slideNumber: false,
	        // Push each slide change to the browser history
	        history: false,
	        // Enable keyboard shortcuts for navigation
	        keyboard: false,
	        // Enable the slide overview mode
	        overview: true,
	        // Vertical centering of slides
	        center: true,
	        // Enables touch navigation on devices with touch input
	        touch: true,
	        // Loop the presentation
	        loop: false,
	        // Change the presentation direction to be RTL
	        rtl: false,
	        // Turns fragments on and off globally
	        fragments: true,
	        // Flags if the presentation is running in an embedded mode,
	        // i.e. contained within a limited portion of the screen
	        embedded: false,
	        // Flags if we should show a help overlay when the questionmark
	        // key is pressed
	        help: true,
	        // Flags if speaker notes should be visible to all viewers
	        showNotes: false,
	        // Number of milliseconds between automatically proceeding to the
	        // next slide, disabled when set to 0, this value can be overwritten
	        // by using a data-autoslide attribute on your slides
	        autoSlide: 0,
	        // Stop auto-sliding after user input
	        autoSlideStoppable: true,
	        // Enable slide navigation via mouse wheel
	        mouseWheel: false,
	        // Hides the address bar on mobile devices
	        hideAddressBar: true,
	        // Opens links in an iframe preview overlay
	        previewLinks: false,
	        // Transition style
	        transition: 'none', // none/fade/slide/convex/concave/zoom
	        // Transition speed
	        transitionSpeed: 'default', // default/fast/slow
	        // Transition style for full page slide backgrounds
	        backgroundTransition: 'none', // none/fade/slide/convex/concave/zoom
	        // Number of slides away from the current that are visible
	        viewDistance: 1,
	        // Parallax background image
	        parallaxBackgroundImage: '', // e.g. "'https://s3.amazonaws.com/hakim-static/reveal-js/reveal-parallax-1.jpg'"
	        // Parallax background size
	        parallaxBackgroundSize: '', // CSS syntax, e.g. "2100px 900px"
	        // Amount to move parallax background (horizontal and vertical) on slide change
	        // Number, e.g. 100
	        parallaxBackgroundHorizontal: '',
	        parallaxBackgroundVertical: ''

	    });



}

function creaTesto(testo)
{
		nasconditutto();
		//var testo = "sdfgsdfg dfgsdfg\nsdfgsdfg dsfgdsf dfgdsfg dfgdsfg\nfdsgsdfg dfgsdfg dfsgdsfg\nsdgsdfgsf\nsdsdgsfdg\nsdsfgdfg\nsdfgdfsgdfg\nsdgsdfgdsfg\nsdfgdfsgdfg\nsdfgsdfg dsfgdsf dfgdsfg dfgdsfg\nfdsgsdfg dfgsdfg dfsgdsfg\nsdgsdfgsf\nsdsdgsfdg\nsdsfgdfg\nsdfgdfsgdfg\nsdgsdfgdsfg\nsdfgdfsgdfg";
		testo = testo.trim();
		//tolgo le virgolette
		//testo = testo.substring(1, (testo.length-1));

		console.log("testo prima: " + testo);
		testo = testo.replace(/(?:\r\n|\r|\n)/g, '<br />');
		//all'inizio e alla fine ci sono le virgolette, le tolgo
		console.log("testo dopo: " + testo);

		$("#casellaTesto").html(testo);
		var altezzaHeader = $("#header").height();
		var altezzaFooter = $("#footerMsg").height();
		var altezza=$( window ).height();
		var altezzaDisponibile=altezza-altezzaHeader-altezzaFooter;
		var altezzaTesto=altezzaDisponibile*0.6;
		var posizione=(altezzaDisponibile*0.15) + altezzaHeader;


		$("#casellaTesto").css("height",altezzaTesto+"px");
		$("#textContainer").css("top",posizione+"px");
		$("#textContainer").show();

		$("#backButton").hide();


}


function disabilitatutto()
{
	if (logEnabled==true) AR.logger.info("disabilita");
	console.log("sono in disabilitatutto");
    
	for (i=0;i<arrayOverlay.length;i++)
	{
		arrayOverlay[i].enabled=false;
		arrayOverlay[i].scale=0;
	}

	/*
	for (i=0;i<arrayAnim.length;i++)
	{
		arrayAnim[i].stop();
		arrayAnim[i].destroy();
	}
	arrayAnim=[];
	*/

	//$("#rettangolo").show();
	//$("#linea").show();
	//$("#griglia").show();


}


function nasconditutto()
{
	if (logEnabled==true) AR.logger.info("nasconditutto");
	console.log("sono in nasconditutto");
    
	for (i=0;i<arrayOverlay.length;i++)
	{
		arrayOverlay[i].opacity=0;
	}

	/*
	for (i=0;i<arrayAnim.length;i++)
	{
		arrayAnim[i].stop();
		arrayAnim[i].destroy();
	}
	arrayAnim=[];
	*/

	//$("#rettangolo").show();
	//$("#linea").show();
	//$("#griglia").show();


}

function mostratutto()
{
	if (logEnabled==true) AR.logger.info("mostratutto");
	console.log("sono in mostratutto");
    
	for (i=0;i<arrayOverlay.length;i++)
	{
		arrayOverlay[i].opacity=1;
	}

	//if (livelloAttuale==2) $("#returnFirstLevel").show();
	//$("#returnFirstLevel").show();
}


var myOffsetX=0.20;
var myOffsetY=0.20;
var elementIndex=0;


function back()
{

	mostratutto();

	//abilitaOverlay(true);

	//ha premuto back... cosa ci stava sullo schermo? un video o lo slideshow?
	if ($("#slideShow").is(':visible'))
	{
		$("#slideShow").hide();
	}

	var htmlToAdd="";
	$("#videoContainer").html(htmlToAdd);
	$("#videoContainer").hide();



	//$("#image").attr("src","");
	$("#imageContainer").hide();
	$("#textContainer").hide();

	//$("#frecciaSx").hide();
	//$("#frecciaDx").hide();
	$("#backButton").show();
	//$("#frecciaSx").unbind('click');
	//$("#frecciaDx").unbind('click');
}


function closeImage ()
{
	console.log("Sono nello close immagine: ");

	var htmlToAdd="";
	$("#imageContainer").html(htmlToAdd);
	$("#imageContainer").hide();

}


function testVideo()
{
		nasconditutto();
		var urlVideo = "";
		urlVideo="6B.mp4";

		//quanto lo devo fare grande il video?
		var larghezzaDiv=$("#videoContainer").width();


		var htmlToAdd="";
		htmlToAdd += "<video id='videoInterno' autoplay controls width='"+larghezzaDiv*0.95+"' style='padding-top:"+larghezzaDiv*0.025+"px;padding-bottom:"+larghezzaDiv*0.025+"px;'>";
		
		//su alcuni device non riesce a prendere il video da assets. percio' si deve mettere sul device (in downloads per esempio) e puntarlo da li
		//htmlToAdd += "<source src='/sdcard/Download/"+urlVideo+"' type='video/mp4' />";
		htmlToAdd += "<source src='assets/2_0.mp4' type='video/mp4' />";
		
		htmlToAdd += "</video>";
		htmlToAdd += "<image src='assets/btn_close.png' onclick='back();' style='height:80px;border: 0px;background: none;box-shadow: none;margin: 0px;padding: 0px;right: -10px;top: -2px;position:absolute;' />";

		console.log(htmlToAdd);

		$("#videoContainer").html(htmlToAdd);



		var video = document.getElementById('videoInterno');
		video.addEventListener('loadeddata', function() {
		    console.log("Loaded the video's data!");
		    
		    //calcolo l'altezza del contenitore
		    var altezzaVideo = $("#videoInterno")[0].videoHeight;
		    var larghezzaVideo = $("#videoInterno")[0].videoWidth;
		    var larghezzaContenitore=$("#videoContainer").width();

		    //var altezzaContenitore=$("#videoContainer").height();
		    var altezzaContenitore=altezzaVideo/larghezzaVideo*larghezzaContenitore;
			

			var altezzaHeader = $("#header").height();
			var altezzaFooter = $("#footerMsg").height();
			var altezza=$( window ).height();
			var posizione=(altezza-altezzaHeader-altezzaContenitore-altezzaFooter)/2 + altezzaHeader;
			console.log("altezzaVideo: " + altezzaVideo);
			console.log("larghezzaVideo: " + larghezzaVideo);
			console.log("altezzaContenitore: " + altezzaContenitore);
			console.log("altezzaHeader: " + altezzaHeader);
			console.log("altezzaFooter: " + altezzaFooter);
			console.log("altezza: " + altezza);
			console.log("posizione: " + posizione);

			$("#videoContainer").css("top",posizione+"px");
			$("#videoContainer").show();

		}, false);


}

function testTesto()
{
		nasconditutto();
		var testo = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.\nUt enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.\nDuis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.\nExcepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.\nLorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.\nUt enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.\nDuis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.\nExcepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.";
		testo = testo.replace(/(?:\r\n|\r|\n)/g, '<br />');

		$("#casellaTesto").html(testo);
		var altezzaHeader = $("#header").height();
		var altezzaFooter = $("#footerMsg").height();
		var altezza=$( window ).height();
		var altezzaDisponibile=altezza-altezzaHeader-altezzaFooter;
		var altezzaTesto=altezzaDisponibile*0.6;
		var posizione=(altezzaDisponibile*0.15) + altezzaHeader;


		$("#casellaTesto").css("height",altezzaTesto+"px");
		$("#textContainer").css("top",posizione+"px");
		$("#textContainer").show();


}


function testSlideshow()
{

		var arrayImg=[];

		arrayImg.push("vertical.jpg");
		arrayImg.push("2B_it.png");
		arrayImg.push("3-1-ricetta_en.png");
		arrayImg.push("slide.png");



		nasconditutto();
		//grandezza del carattere...
		var larghezza=$( window )[0].innerWidth;
		var altezza=$( window )[0].innerHeight;
		var altezzaHeader = $("#header").height();
		var altezzaFooter = $("#footerMsg").height();
		

		var altezzaImmagine=(altezza-altezzaHeader-altezzaFooter)*0.7;
		

		//var altezzaImmagine=altezza*0.4;
		//if (altezza<=541) altezzaImmagine=altezza*0.8;
		//else if (altezza<=800) altezzaImmagine=altezza*0.5;

		console.log( "larghezza:" + larghezza );
		console.log( "altezza:" + altezza );
		console.log( "altezzaImmagine:" + altezzaImmagine );
		var charSize="100%";

		if (larghezza>1600) charSize="60%";
		else if (larghezza>1500) charSize="60%";
		else if (larghezza>1400) charSize="60%";
		else if (larghezza>1300) charSize="60%";
		else if (larghezza>1200) charSize="60%";
		else if (larghezza>1100) charSize="60%";
		else if (larghezza>1000) charSize="60%";
		else if (larghezza>900) charSize="60%";
		else if (larghezza>800) charSize="60%";
		else if (larghezza>700) charSize="60%";
		else if (larghezza>600) charSize="60%";
		else charSize="60%";


		var htmlToAdd="";

/*
		// ne metto prima una che illustra lo slide
		if (arrayImg.length>1)
		{
			htmlToAdd += "<section>";
			htmlToAdd += "<div style='background-color:white;text-align:center;width: 80%;border: 1px solid #aaaaaa;border-radius: 20px;position: absolute;right: 10%;top:50px;'>";
			htmlToAdd += "<image src='assets/slide.png' style='width:60%;margin-bottom: 0px;border: 0px;background: none;margin: 0px;margin-left:10%;box-shadow: none;' />";
			htmlToAdd += "<div style='clear:both;width:100%;display: inline-flex;'>";
			//htmlToAdd += "<p class='testoMini' style='width:80%;text-align:center;color:#b1c903;margin-top:0%;padding-top: 2%;margin-bottom: 2%;margin-left:10%;border:0px solid green;font-size:"+charSize+"'>Scorri le slide per esaminare le schede dei prodotti</p>";
			htmlToAdd += "</div>";
			htmlToAdd += "</div>";
			htmlToAdd += "</section>";
		}
*/
		for (i=0;i<arrayImg.length;i++)
		{
			htmlToAdd += "<section>";
			htmlToAdd += "<div style='background-color:white;text-align:center;width: 80%;border: 1px solid #aaaaaa;border-radius: 20px;position: absolute;right: 10%;top:0px;'>";
			//htmlToAdd += "<p class='menuTitle' style='text-align:center;color:#b1c903;margin-top:3%;margin-bottom: 0px;font-size:80%;'>" + arrayTitoli[i] + "</p>";
			htmlToAdd += "<image src='assets/"+ arrayImg[i] + "' style='height:"+altezzaImmagine+"px;margin-bottom: 0px;border: 0px;background: none;margin: 0px;box-shadow: none;margin-top: 2%;' />";
			htmlToAdd += "<image src='assets/btn_close.png' onclick='back();' style='height:80px;border: 0px;background: none;box-shadow: none;margin: 0px;padding: 0px;right: -10px;top: -2px;position:absolute;' />";
			//htmlToAdd += "<div style='clear:both;width:100%;display: inline-flex;'>";
			//htmlToAdd += "</div>";
			htmlToAdd += "</div>";
			htmlToAdd += "</section>";


		}
		console.log(htmlToAdd);

		$("#mySlides").html(htmlToAdd);
		$("#slideShow").show();
		

		//$("#backButton").show();

		var altezzaHeader = $("#header").height();
		//var larghezzaImg = $("#imgChiudi").width();
		$("#slideShow").css("top",altezzaHeader);
		//$("#labelChiudi").css("font-size",altezzaHeader*0.6);
		//$("#labelChiudi").css("margin-top",altezzaHeader*0.2);
		//$("#labelChiudi").css("left",larghezzaImg+"px");


	    Reveal.addEventListener( 'ready', function( event ) {
	    	console.log("Reveal Ready");
	    	Reveal.slide( 0 );
	    	Reveal.configure({ transition: 'slide' });
		} );

	    Reveal.initialize({
	        margin: 0.1,
	        //override width
	        height: (altezza-altezzaHeader-altezzaFooter),
	        //height: 550,
	        
	        //override width
	        width: larghezza,
	        //width: 940,
	        // Display controls in the bottom right corner
	        controls: false,
	        // Display a presentation progress bar
	        progress: false,
	        // Display the page number of the current slide
	        slideNumber: false,
	        // Push each slide change to the browser history
	        history: false,
	        // Enable keyboard shortcuts for navigation
	        keyboard: false,
	        // Enable the slide overview mode
	        overview: true,
	        // Vertical centering of slides
	        center: true,
	        // Enables touch navigation on devices with touch input
	        touch: true,
	        // Loop the presentation
	        loop: false,
	        // Change the presentation direction to be RTL
	        rtl: false,
	        // Turns fragments on and off globally
	        fragments: true,
	        // Flags if the presentation is running in an embedded mode,
	        // i.e. contained within a limited portion of the screen
	        embedded: false,
	        // Flags if we should show a help overlay when the questionmark
	        // key is pressed
	        help: true,
	        // Flags if speaker notes should be visible to all viewers
	        showNotes: false,
	        // Number of milliseconds between automatically proceeding to the
	        // next slide, disabled when set to 0, this value can be overwritten
	        // by using a data-autoslide attribute on your slides
	        autoSlide: 0,
	        // Stop auto-sliding after user input
	        autoSlideStoppable: true,
	        // Enable slide navigation via mouse wheel
	        mouseWheel: false,
	        // Hides the address bar on mobile devices
	        hideAddressBar: true,
	        // Opens links in an iframe preview overlay
	        previewLinks: false,
	        // Transition style
	        transition: 'none', // none/fade/slide/convex/concave/zoom
	        // Transition speed
	        transitionSpeed: 'default', // default/fast/slow
	        // Transition style for full page slide backgrounds
	        backgroundTransition: 'none', // none/fade/slide/convex/concave/zoom
	        // Number of slides away from the current that are visible
	        viewDistance: 1,
	        // Parallax background image
	        parallaxBackgroundImage: '', // e.g. "'https://s3.amazonaws.com/hakim-static/reveal-js/reveal-parallax-1.jpg'"
	        // Parallax background size
	        parallaxBackgroundSize: '', // CSS syntax, e.g. "2100px 900px"
	        // Amount to move parallax background (horizontal and vertical) on slide change
	        // Number, e.g. 100
	        parallaxBackgroundHorizontal: '',
	        parallaxBackgroundVertical: ''

	    });



}

function getLabel(codice,lingua,defaultLabel)
{
	console.log("getLabel - " + codice + " - " + lingua + " - " + defaultLabel);
	var ret = defaultLabel;
	//cerco il codice nell'oggetto
	for (var i=0;i<labelsObj.length;i++)
	{
		if (labelsObj[i].codice==codice)
		{
			switch(lingua) {
			    case "it":
			        if ((labelsObj[i].it==null) || (labelsObj[i].it=="")) ret=labelsObj[i].itDefault;
			        else ret=labelsObj[i].it;
			        break;
			    case "en":
			        if ((labelsObj[i].en==null) || (labelsObj[i].en=="")) ret=labelsObj[i].enDefault;
			        else ret=labelsObj[i].en;
			        break;
			}
		}

	}
	return ret;

}


$(document).ready(function()
{
	//testVideo();
	//testTesto();
	//scompareRettangolo();
	//muoviLineaDx();
	
	//testSlideshow();
	//$( "#rettangolo" ).hide();

/*
			$("#myPano").pano({
				img: "./panorama/sphere.jpg"
			});
*/




});
	
