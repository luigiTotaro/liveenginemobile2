var World = {
	loaded: false,
	rotating: false,


	init: function initFn() {
		this.loaded=false;
		//AR.context.destroyAll();
		this.createOverlays();
	},

	createOverlays: function createOverlaysFn() {
		/*
			First an AR.ClientTracker needs to be created in order to start the recognition engine. It is initialized with a URL specific to the target collection. Optional parameters are passed as object in the last argument. In this case a callback function for the onLoaded trigger is set. Once the tracker is fully loaded the function worldLoaded() is called.

			Important: If you replace the tracker file with your own, make sure to change the target name accordingly.
			Use a specific target name to respond only to a certain target or use a wildcard to respond to any or a certain group of targets.

			Adding multiple targets to a target collection is straightforward. Simply follow our Target Management Tool documentation. Each target in the target collection is identified by its target name. By using this target name, it is possible to create an AR.Trackable2DObject for every target in the target collection.
		*/



		//alert("assets/"+oggetto.tracker);

		this.tracker = new AR.ClientTracker("assets/tracker.wtc", {
			onLoaded: this.worldLoaded
		});
		
		//this.rotationAnimation = new AR.PropertyAnimation(this.modelCar, "rotate.roll", -25, 335, 10000);

		var dimensioneIndicatore = 0.04;
		var dimensioneSottoIndicatore = 0.07;
		arrayOverlay = [];
		arrayAnimations = [];

		//pulsante play
		var imgTemp = new AR.ImageResource("assets/pulsante.png");
		var overlay = new AR.ImageDrawable(imgTemp, 0.15,
		{
			offsetX: -0.0,
			offsetY: 0.0,
			enabled: true
		});
		overlay.imageRef="play";
		overlay.type="image";
		overlay.onClick = this.showContents();


		var animation1 = new AR.PropertyAnimation(overlay, "scaling", 1, 1.3, 750, 
		 {type: AR.CONST.EASING_CURVE_TYPE.EASE_IN_OUT_QUAD});
		var animation2 = new AR.PropertyAnimation(overlay, "scaling", 1.3, 1, 750,
		 {type: AR.CONST.EASING_CURVE_TYPE.EASE_IN_OUT_QUAD});

		var arr_anims = new Array(
		   animation1,
		   animation2
		   );

		animGroupSmiley = new AR.AnimationGroup(
		       AR.CONST.ANIMATION_GROUP_TYPE.SEQUENTIAL,
		       arr_anims);

		animGroupSmiley.start(-1);
		arrayOverlay.push(overlay);

		//contorno
		var imgTemp = new AR.ImageResource("assets/bordoDx.png");
		var overlay = new AR.ImageDrawable(imgTemp, 1.0,
		{
			offsetX: 0.2,
			offsetY: 0.0,
			enabled: false,
			horizontalAnchor : AR.CONST.HORIZONTAL_ANCHOR.RIGHT,	
		});
		overlay.imageRef="bordoDx";
		overlay.type="image";
		arrayOverlay.push(overlay);
		
		var imgTemp = new AR.ImageResource("assets/bordoSx.png");
		var overlay = new AR.ImageDrawable(imgTemp, 1.0,
		{
			offsetX: -0.2,
			offsetY: 0.0,
			enabled: false,
			horizontalAnchor : AR.CONST.HORIZONTAL_ANCHOR.LEFT,	
		});
		overlay.imageRef="bordoSx";
		overlay.type="image";
		arrayOverlay.push(overlay);

/*
		var label = new AR.Label("TEST", 0.08, {
			offsetX: -0.25,
			offsetY: -0.2,
			scale: 1,
			opacity : 0.7,
			style : {
				textColor : '#000000', 
				backgroundColor : '#FFFFFF' 
			}
		});
		label.imageRef="playLabel";
		label.type="label";
		arrayOverlay.push(label);


		//pulsante play
		var imgTemp = new AR.ImageResource("assets/tape.png");
		var overlay = new AR.ImageDrawable(imgTemp, 0.3,
		{
			offsetX: 0.25,
			offsetY: 0.0,
			enabled: true,
		});
		overlay.imageRef="misure";
		overlay.type="image";
		arrayOverlay.push(overlay);
		overlay.onClick = this.showImages();

		var label = new AR.Label("Misure", 0.08, {
			offsetX: 0.25,
			offsetY: -0.2,
			scale: 1,
			opacity : 0.7,
			style : {
				textColor : '#000000', 
				backgroundColor : '#FFFFFF' 
			}
		});
		label.imageRef="misureLabel";
		label.type="label";
		arrayOverlay.push(label);
*/

		var modelManiglia = new AR.Model("assets/brillo.wt3", {
			scale: {
				x: 0.01,
				y: 0.01,
				z: 0.01
			},
			translate: {
				x: 0.0,
				y: 0.0,
				z: 0.0
			},
			rotate: {
				roll: 90.0,
			    tilt: 0.0,
			    heading: 0.0

			},
			enabled:false
		});
		modelManiglia.imageRef="3d";
		modelManiglia.type="3d";
		arrayOverlay.push(modelManiglia);

		rotationAnimation = new AR.PropertyAnimation(modelManiglia, "rotate.roll", 0, 360, 7000);

		

			var trackable = new AR.Trackable2DObject(this.tracker, "*", {
				drawables: {
					cam: arrayOverlay
				},
				onEnterFieldOfVision: function onEnterFieldOfVisionFn(a)
	            {
					console.log("ho rilevato " + a);
					//console.log("aspectRatio: "+trackable.aspectRatio);
					var nome="";
					if (a.search("copertina")!=-1)
					{
						nome="Andy Warhol Prints"
						itemRilevato="copertina";
					}
					else if (a.search("reginaElisabetta")!=-1)
					{
						nome="Queen Elizabeth II, (1985)"
						itemRilevato="reginaElisabetta";
					}
					else if (a.search("marilyn")!=-1)
					{
						nome="Marilyn Monroe, (1967)"
						itemRilevato="marilyn";
					}
	
					var cssDivInstructions = " style='display: table-cell;vertical-align: middle; text-align: center; width: 50%; padding-right: 15px;'";
					document.getElementById('loadingMessage').innerHTML ="<div" + cssDivInstructions + ">" + nome + "</div>";
					//in base al nome dell'immagine rilevata, decido quali elementi mostrare
					//facciamo finta che il livello trovato é 01
					

					//impostaNomeVideo();


					scegliOverlay();



					trackable.snapToScreen.enabled = false;
				},
				snapToScreen: {
					enabledOnExitFieldOfVision: false,
					snapContainer: document.getElementById('snapContainer')
				},	
				onExitFieldOfVision: function onExitFieldOfVisionFn(a)
	            {
					console.log("ho perso la rilevazione di " + a);
					var cssDivInstructions = " style='display: table-cell;vertical-align: middle; text-align: center; width: 50%; padding-right: 15px;'";
					document.getElementById('loadingMessage').innerHTML ="<div" + cssDivInstructions + ">Looking for Target.....</div>";

					disabilitatutto();
					itemRilevato="";	
					trackable.snapToScreen.enabled = false;
				},
			});

			//arrayTrackable.push(trackable);

			//test... dopo che l'ho caricato, devo rimettere a posto l'offsetx di ogni elemento... visto che è calcolato in sdu, dove 1 sdu è l'altezza dell'immagine trovata...
			//console.log("tracker impostato, aspectRatio: "+trackable.aspectRatio);

			//console.log("finito con " + trackable.targetName);
			//console.log("Array indici: "+ JSON.stringify(arrayIndiciIndicatoriGlobale));


	//	}




	},


	toggleAnimateModel: function toggleAnimateModelFn()
	{
		if (!World.rotationAnimation.isRunning()) {
			if (!World.rotating) {
				// Starting an animation with .start(-1) will loop it indefinitely.
				World.rotationAnimation.start(-1);
				World.rotating = true;
			} else {
				// Resumes the rotation animation
				World.rotationAnimation.resume();
			}
		} else {
			// Pauses the rotation animation
			World.rotationAnimation.pause();
		}

		return false;
	},

	mostraSottoIndicatori: function (indexGlobale, index)
	{
        return function() {
			//da qui in poi devo mostrare (o nascondere) i sottoindicatori che appartengono a quell'indicatore...
			//come faccio? l'indicatore ha zOrder 10, gli altri di piu... mostro tutti gli oggetti con zOrder 20 (i sotto indicatori) fino a che non incontro un altro zOder 10 (un altro indicatore)
			var timeoutValue=0; //timeout iniziale per mostare i sotto elementi

			var toEnable=false;
			console.log("sono in mostraSottoIndicatori, con index: "+index);
			
			//console.log("zOrder iniziale: " + arrayOverlayGlobale[indexGlobale][indexCheck].zOrder);

			//faccio dei cicli while prendendo come indice di inizio gli indici memorizzati nell'arrayIndiciIndicatoriGlobale
			//su quello interessato scelgo cosa fare, sugli altri disabilito tutto
			
			//lo faccio una prima volta solo per quello selezionato, in modo da sapere se ho ri-cliccato per togliere gli indicatore
			for (var i=0;i<arrayIndiciIndicatoriGlobale[indexGlobale].length;i++)
			{
				var startingIndex=arrayIndiciIndicatoriGlobale[indexGlobale][i][0];
				console.log("Estratto l'indice: "+startingIndex);
				
				//è questo quello cliccato?
				if (startingIndex==index)
				{
					console.log("E' quello cliccato");
					var indexCheck=index+1;
					//vedo se li devo abilitare o meno...
					if (arrayOverlayGlobale[indexGlobale][indexCheck].enabled==true) toEnable=false;
					else toEnable=true;

					console.log("grandezza array: " + arrayOverlayGlobale[indexGlobale].length);
					while((indexCheck<arrayOverlayGlobale[indexGlobale].length) && (arrayOverlayGlobale[indexGlobale][indexCheck].zOrder!=10))
					{
						console.log("Processo l'indice: " + indexCheck);
						//console.log("sono dentro al while - zOrder: " + arrayOverlayGlobale[indexGlobale][indexCheck].zOrder);
						if (arrayOverlayGlobale[indexGlobale][indexCheck].zOrder==21)
						{
							abilitaOggetto(indexGlobale,indexCheck, timeoutValue,toEnable);
						}
						if (arrayOverlayGlobale[indexGlobale][indexCheck].zOrder==20)
						{
							abilitaOggetto(indexGlobale,indexCheck, timeoutValue,toEnable);
							timeoutValue=timeoutValue+200;
							
/*							if (arrayOverlayGlobale[indexGlobale][indexCheck].enabled==true)
							{
								toEnable=false;

								abilitaOggetto(indexGlobale,indexCheck, timeoutValue,toEnable);
								timeoutValue=timeoutValue+200;
								//arrayOverlayGlobale[indexGlobale][indexCheck].enabled=false;
							}
							else
							{
								toEnable=true;
								//lo devo mostrare con un intervallo
								abilitaOggetto(indexGlobale,indexCheck, timeoutValue,toEnable);
								timeoutValue=timeoutValue+200;
							}
*/						}

/*						if (arrayOverlayGlobale[indexGlobale][indexCheck].zOrder==40) //la label
						{
							if (toEnable==true) arrayOverlayGlobale[indexGlobale][indexCheck].enabled=false;
							else arrayOverlayGlobale[indexGlobale][indexCheck].enabled=true;
						}				
*/						indexCheck++;

					}

				}

			}

			if (toEnable==true) indiceIndicatoreCorrente=index;
			else indiceIndicatoreCorrente=-1;

			//ora lo faccio una seconda volta solo per quelli non selezionati.. se avevo selezionato quello attivo tolgo tutto, se avevo deselezionato quello attivo, rimetto le label
			for (var i=0;i<arrayIndiciIndicatoriGlobale[indexGlobale].length;i++)
			{
				var startingIndex=arrayIndiciIndicatoriGlobale[indexGlobale][i][0];
				console.log("Estratto l'indice: "+startingIndex);
				
				//è questo quello cliccato?
				if (startingIndex!=index)
				{
					console.log("Non è quello cliccato");
					//ma era abilitato in principio, l'indicatore (era su un livello visibile)?
					if (arrayOverlayGlobale[indexGlobale][startingIndex].enabled==true)
					{
						var indexCheck=startingIndex+1;
						while((indexCheck<arrayOverlayGlobale[indexGlobale].length) && (arrayOverlayGlobale[indexGlobale][indexCheck].zOrder!=10))
						{
							//cosa ho fatto sull'elemento selezionato?
	/*						if (toEnable==true)
							{	
								//tolgo tutto
								//console.log("sono dentro al while - zOrder: " + arrayOverlayGlobale[indexGlobale][indexCheck].zOrder);
								arrayOverlayGlobale[indexGlobale][indexCheck].enabled=false;
								indexCheck++;
							}
							else
							{
	*/							//tolgo tutto ma non la label
								if (arrayOverlayGlobale[indexGlobale][indexCheck].zOrder==15) //la label
								{
									arrayOverlayGlobale[indexGlobale][indexCheck].enabled=true;
								}
								else arrayOverlayGlobale[indexGlobale][indexCheck].enabled=false;
								indexCheck++;
	//						}
						}
					}

				}

			}

			$("#backButton").hide(); //in caso ci stava qualcosa di visuyalizzato
			$("#imageContainer").hide(); //in caso ci stava una immagine visualizzata



        };
	},

	mostraParte: function (parte)
	{
        return function() {
			//alert(url);
			console.log("Sono nella mostraParte : "+parte);
			ricaricaParti (parte);
        };
	},


	mostraVideo: function (url)
	{
        return function() {
			//alert(url);
			//var vidonetopay1 = new AR.context.startVideoPlayer("assets/"+url);
			//alert(parameter);
			if (logEnabled==true) AR.logger.info("start video: "+ parameter + "/" + url);
			var vidonetopay1 = new AR.context.startVideoPlayer(parameter + "/" + url);
        };
	},

	mostraVideoOverlay: function (indexGlobale, index)
	{
        return function() {
			//alert(url);
			//var vidonetopay1 = new AR.context.startVideoPlayer("assets/"+url);
			//alert(parameter);
			indexVideo=index+2; //su index ci sta l'indicatore, su index+1 ci sta la label e su index+2 il video vero e proprio...
			if (logEnabled==true) AR.logger.info("sono nel video overlay: " + indexGlobale + " - " + index);
			console.log("sono nel video overlay: " + indexGlobale + " - " + index);
			//il video è mostrato?
			if (arrayOverlayGlobale[indexGlobale][indexVideo].enabled==true)
			{
				console.log("il video è visibile");
				//lo stoppo 
				//arrayOverlayGlobale[indexGlobale][indexVideo].enabled=false;
				arrayOverlayGlobale[indexGlobale][indexVideo].pause();
				arrayOverlayGlobale[indexGlobale][indexVideo].playing = false;
				//faccio un ciclo... tutto cio' che sta a un zOrder di 10 (gli indicatori e le label) vengono rimessi, gli altri no
				for (t=0;t<arrayOverlayGlobale[indexGlobale].length;t++)
				{
					if (arrayOverlayGlobale[indexGlobale][t].zOrder==10) arrayOverlayGlobale[indexGlobale][t].enabled=true;
					else arrayOverlayGlobale[indexGlobale][t].enabled=false;
				}


				//e rimetto l'indicatore che avevamo tolto
				//arrayOverlayGlobale[indexGlobale][indexVideo+2].enabled=true;
			}
			else
			{
				//mostro il video
				console.log("il video non è visibile");
				//tolgo TUTTI gli elementi di quell'indicatore
				var indexCheck=indiceIndicatoreCorrente+1;
				while((indexCheck<arrayOverlayGlobale[indexGlobale].length) && (arrayOverlayGlobale[indexGlobale][indexCheck].zOrder!=10))
				{
					console.log("Processo l'indice: " + indexCheck);
					//console.log("sono dentro al while - zOrder: " + arrayOverlayGlobale[indexGlobale][indexCheck].zOrder);
					if (arrayOverlayGlobale[indexGlobale][indexCheck].zOrder==20)
					{
						arrayOverlayGlobale[indexGlobale][indexCheck].enabled=false;
					}
					indexCheck++;
				}
				
				arrayOverlayGlobale[indexGlobale][indexVideo].enabled=true;
				arrayOverlayGlobale[indexGlobale][indexVideo].play();
				arrayOverlayGlobale[indexGlobale][indexVideo].playing = true;
				//arrayOverlayGlobale[indexGlobale][indexVideo+1].enabled=true;
				//metto il pulsante per tornare indietro
				
				$("#backButton").show();	
				var altezzaHeader = $("#header").height();
				$("#slideShow").css("top",altezzaHeader);
				$("#labelChiudi").css("font-size",altezzaHeader*0.6);
				$("#labelChiudi").css("margin-top",altezzaHeader*0.2);
				$("#labelChiudi").css("left",$("#imgChiudi").width());

			
				
				//e tolgo l'indicatore 
				//arrayOverlayGlobale[indexGlobale][indexVideo+2].enabled=false;
			}
        };
	},

	mostraPdf: function (url)
	{
        return function() {
			//alert(url);
			var pdf = new AR.context.openInBrowser(url,false);
        };
	},

	showImage: function (url1, url2)
	{
        return function() {
			//alert(url);
			console.log("Sono nello show image : "+url1 + " - " + url2);
			//showImage (parameter + "/" + url);
			showImage (url1, url2);
        };
	},
	
	showImages: function ()
	{
        return function() {
			//alert(url);
			console.log("Sono nello show images");
			//showImage (parameter + "/" + url);
			showImages ();
        };
	},

	showVideo: function ()
	{
        return function() {
			//alert(url);
			console.log("Sono nello show video");
			//showImage (parameter + "/" + url);
			showVideo ();
        };
	},

	showContents: function ()
	{
        return function() {
			//alert(url);
			console.log("Sono nello showContents");
			//showImage (parameter + "/" + url);
			showContents ();
        };
	},

	showHtmlPage: function (url)
	{
        return function() {
			//alert(url);
			console.log("Sono nello show html page : "+url);
			showHtml(parameter + "/" + url);
			//AR.context.openInBrowser ( parameter + "/" + url, true);
        };
	},


	showImagesSeries: function (imageBase,extension,numImages)
	{
        return function() {
			//alert(url);
			console.log("Sono nello show image series: "+imageBase);
			showFirstImage (imageBase,extension,numImages);
        };
	},

/*


    planetClicked: function(planet) {
        return function() {
            document.getElementById("info").setAttribute("class", "info");
            document.getElementById("name").innerHTML = planet.name;
            document.getElementById("mass").innerHTML = planet.mass;
            document.getElementById("diameter").innerHTML = planet.diameter;
            document.getElementById("info").setAttribute("class", "infoVisible");
        };
    }
*/

	worldLoaded: function worldLoadedFn() {
		console.log("worldLoaded");
		var cssDivInstructions = " style='display: table-cell;vertical-align: middle; text-align: center; width: 50%; padding-right: 15px;'";
		document.getElementById('loadingMessage').innerHTML =
			"<div" + cssDivInstructions + ">Looking for Target.....</div>";

		// Remove Scan target message after 10 sec.
		setTimeout(function() {
			//var e = document.getElementById('loadingMessage');
			var cssDivInstructions = " style='display: table-cell;vertical-align: middle; text-align: center; width: 50%; padding-right: 15px;'";
			document.getElementById('loadingMessage').innerHTML ="<div" + cssDivInstructions + ">Looking for Target.....</div>";
			//e.parentElement.removeChild(e);
		}, 1000);
	}



};


//var parameter = "file:///var/mobile/Containers/Data/Application/FDB98D9D-BFC3-4497-A720-81E52BA126AB/Documents/repository";

var logEnabled= false;

if (logEnabled==true)
{
AR.logger.activateDebugMode();
}
//AR.logger.info(parameter)

//loadDownloadedJson(parameter + "/definizione.json");

console.log("sono qui dentro");
var fase="";


function passData(id,localFase)
{   
	//fase="Inst"; //per debug, installazione



	//quento è grande la finestra?
	console.log("window.height: "+$(window).height());   // returns height of browser viewport
	console.log("document.height: "+$(document).height()); // returns height of HTML document
	console.log("window.width: "+$(window).width());   // returns width of browser viewport
	console.log("document.width: "+$(document).width()); 


	console.log("v 1.0 - passData: "+id);
	if (logEnabled==true) AR.logger.info("v 1.0 - passData: "+id);

	parameter = id;
	//alert("prima: "+parameter);
	
	//loadDownloadedJson(parameter + "/definizione.json");
	//aspetto che scelga la fase....
	console.log("v 1.0 - fase: "+localFase);

	$("#sceltaFase").hide();

	//$("#slideShow").hide();


	//$("#backButton").hide();

	var larghezza=$( window ).width();
	var altezza=$( window ).height();
	var altezzaHeader=altezza*0.05;

	$("#header").height(altezzaHeader+"px");

	$("#labelChiudi").css("font-size",altezzaHeader*0.6);
	$("#labelChiudi").css("margin-top",altezzaHeader*0.2);
	$("#labelChiudi").css("left",$("#imgChiudi").width());
	
	$("#backButton").css("height",altezzaHeader);
	$("#backButton").css("width",larghezza*0.2);




	loadInternalJson();

	//World.init();

/*	if (localFase==1)
	{
		fase="Inst";
		$("#faseLabel").html("Installazione");
		$("#sceltaFase").hide();
		loadDownloadedJson(parameter + "/definizione.json");
	}
	else if (localFase==2)
	{
		fase="MiS"; 
		$("#faseLabel").html("Messa in Servizio");
		$("#sceltaFase").hide();
		loadDownloadedJson(parameter + "/definizione.json");
	}
	else if (localFase==3)
	{
		fase="Man"; 
		$("#faseLabel").html("Manutenzione");
		$("#sceltaFase").hide();
		loadDownloadedJson(parameter + "/definizione.json");
	}
*/	


}

var itemRilevato="";


var	oggetto = new Array();
var	oggettoTemp = new Array();
var	arrayOverlay = new Array();
var	arrayOverlayGlobale = new Array(); //contiene n arrayOverlay, uno per ogni immagine da riconoscere nel tracker....
var	arrayIndiciIndicatori = new Array(); //contiene gli indici degli indicatori principali nel'arrayOverlay
var	arrayIndiciIndicatoriGlobale = new Array(); //contiene n arrayIndiciIndicatori, uno per ogni immagine da riconoscere nel tracker....
var	indiceIndicatoreCorrente = -1;
var	arrayCorrispondenzeNomi = new Array(); //contiene n nomi, che mi indicano a quale oggetto dell'array globale mi riferisco
var	arrayTrackable = new Array();
var arrayAnimations = new Array();
var rotationAnimation;

var arrayIndiciOverlay_IndiciOggetto = new Array(); //questo array mi mette in relazione l'indice dell'oggetto overlay con l'ondice in oggetto (la variabile)

var videoIsPlaying=false;
var actualVideo=null;


var inquadratura = 0; //0=tutta la caldaia - 1=superiore - 2=inferiore

//window.appRootDirName = "repository";
//window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, gotFS, fail);

//$( window ).click(function(){
//  alert($(this).attr('id'));
//});


$( window ).resize(function() {
  var larghezza=$( window ).width();
  var altezza=$( window ).height();
	  
	console.log( "larghezza:" + larghezza );
	console.log( "altezza:" + altezza );

	var altezzaHeader = $("#header").height();
	$("#slideShow").css("top",altezzaHeader);
	$("#labelChiudi").css("font-size",altezzaHeader*0.6);
	$("#labelChiudi").css("margin-top",altezzaHeader*0.2);
	$("#labelChiudi").css("left",$("#imgChiudi").width());

	$("#backButton").css("height",altezzaHeader);
	$("#backButton").css("width",larghezza*0.2);

	if (larghezza>altezza)
	{
	//landscape
/*	    $("#inquadratura").css("width",larghezzaDiv);
	    $("#inquadratura").css("left",(larghezza-larghezzaDiv)/2);
	    $("#inquadratura").css("height",altezzaDiv);
	    $("#inquadratura").css("top",(altezza-altezzaDiv)/2);
*/	}
	else
	{
	}

  

});




function gotFS(fileSystem) {
    window.fileSystem=fileSystem;
}

function fail() {
	console.log("failed to get filesystem");
}


function dirReady(entry) {
	console.log("dirReady");
	
	//path del file json scaricato
	//window.fileSystem.root.toURL() + window.appRootDirName + "/definizione.json";
	loadDownloadedJson(window.fileSystem.root.toURL() + window.appRootDirName + "/definizione.json");
}


function ricaricaParti(parte)
{
	if (parte=="sopra") inquadratura=1; //parte superiore
	else inquadratura=2; //parte inferiore
	loadDownloadedJson(parameter + "/definizione.json");

}

function findAnim(riferimento)
{
	var ret;
	console.log("devo cercare l'anim di: " + riferimento);
	for (i=0;i<arrayAnimations.length;i++)
	{
		if (arrayAnimations[i].imageRef==riferimento) ret=arrayAnimations[i];
	}
	console.log("ho trovato: " + ret);
	return ret;

}

function abilitaLivello(nomeRilevato)
{
	if (logEnabled==true) AR.logger.info("abilita nome: "+ nomeRilevato);
	console.log("sono in abilitaLivello, con nome: "+nomeRilevato);
    
    timeoutValue=0;

	for (i=0;i<arrayOverlay.length;i++)
	{
		if (arrayOverlay[i].imageRef==nomeRilevato)
		{
			abilitaOggetto(i,timeoutValue);
			timeoutValue=timeoutValue+250;
		}
		else arrayOverlay[i].enabled=false;
	}	


}


function abilitaOverlay(toEnable)
{
	if (logEnabled==true) AR.logger.info("abilita abilitaOverlay: "+ itemRilevato);
	console.log("sono in abilitaOverlay, con nome: "+itemRilevato);
    
	
	if (toEnable) scegliOverlay();
	else
	{
		//abilito a seconda della maniglia rilevata
		for (i=0;i<arrayOverlay.length;i++)
		{
			arrayOverlay[i].enabled=toEnable;
		}
		rotationAnimation.stop();	
	}
}

function scegliOverlay()
{
	if (logEnabled==true) AR.logger.info("abilita abilitaOverlay: "+ itemRilevato);
	console.log("sono in abilitaOverlay, con nome: "+itemRilevato);
    
	$("#rettangolo").hide();
	//$("#linea").hide();
	//$("#griglia").hide();	


	//abilito a seconda dell'oggetto
	for (i=0;i<arrayOverlay.length;i++)
	{
		if (itemRilevato=="copertina")
		{
			if (arrayOverlay[i].type=="3d")
			{
				arrayOverlay[i].enabled=true;
				rotationAnimation.start(-1);
				//e in più faccio uscire i dettagli
		        $("#textContainer").css("right","-35%");
		        $("#textContainer").show();
		        $("#textContainer" ).animate({
		            right: "5px"
		          }, 1000, function() {
		            // Animation complete.
		          });				

			}
			else arrayOverlay[i].enabled=false;
		}
		else
		{
			if (arrayOverlay[i].type=="3d") arrayOverlay[i].enabled=false;
			else
			{
				arrayOverlay[i].enabled=true;
				//se è il contorno aggiusto la larghezza
				if (arrayOverlay[i].imageRef=="bordoDx")
				{
					if (itemRilevato=="marilyn") arrayOverlay[i].offsetX=0.667*0.5+0.01;
					if (itemRilevato=="reginaElisabetta") arrayOverlay[i].offsetX=0.806*0.5+0.01;
					var animation = new AR.PropertyAnimation(arrayOverlay[i], "offsetX", arrayOverlay[i].offsetX*2, arrayOverlay[i].offsetX, 750,  {type: AR.CONST.EASING_CURVE_TYPE.EASE_OUT_CIRC});
					var arr_anims = new Array(animation);
					animGroupSmiley = new AR.AnimationGroup(AR.CONST.ANIMATION_GROUP_TYPE.SEQUENTIAL,arr_anims);
					animGroupSmiley.start();
				}
				if (arrayOverlay[i].imageRef=="bordoSx")
				{
					if (itemRilevato=="marilyn") arrayOverlay[i].offsetX=-0.667*0.5;
					if (itemRilevato=="reginaElisabetta") arrayOverlay[i].offsetX=-0.806*0.5;
					var animation2 = new AR.PropertyAnimation(arrayOverlay[i], "offsetX", arrayOverlay[i].offsetX*2, arrayOverlay[i].offsetX, 750,  {type: AR.CONST.EASING_CURVE_TYPE.EASE_OUT_CIRC});
					var arr_anims2 = new Array(animation2);
					animGroupSmiley2 = new AR.AnimationGroup(AR.CONST.ANIMATION_GROUP_TYPE.SEQUENTIAL,arr_anims2);
					animGroupSmiley2.start();

				}
				//e se è il pulsante lo sfumo in entrata
				if (arrayOverlay[i].imageRef=="play")
				{
					var animation3 = new AR.PropertyAnimation(arrayOverlay[i], "opacity", 0, 1, 750,  {type: AR.CONST.EASING_CURVE_TYPE.EASE_IN_EXPO});
					var arr_anims3 = new Array(animation3);
					animGroupSmiley3 = new AR.AnimationGroup(AR.CONST.ANIMATION_GROUP_TYPE.SEQUENTIAL,arr_anims3);
					animGroupSmiley3.start();
				}


			}
		}

		
	}	

	



}


function impostaNomeVideo()
{
	if (logEnabled==true) AR.logger.info("impostaNomeVideo nome: "+ itemRilevato);
	console.log("sono in impostaNomeVideo, con nome: "+itemRilevato);
    
	//trovo il nome del video
	var titoloVideo="Nessuno";
	for (i=0;i<oggetto.length;i++)
	{
		if (oggetto[i].nome==itemRilevato) titoloVideo=oggetto[i].titoloVideo;
	}

	for (i=0;i<arrayOverlay.length;i++)
	{
		if (arrayOverlay[i].imageRef=="playLabel")
		{
			arrayOverlay[i].text=titoloVideo;
		}

	}	


}

function showImages()
{
	abilitaOverlay(false);

	if (logEnabled==true) AR.logger.info("showImages nome: "+ itemRilevato);
	console.log("sono in showImages, con nome: "+itemRilevato);


	//grandezza del carattere...
	var larghezza=$( window ).width();
	var altezza=$( window ).height();

	console.log( "larghezza:" + larghezza );
	console.log( "altezza:" + altezza );
	var charSize="100%";

	if (larghezza>1600) charSize="70%";
	else if (larghezza>1500) charSize="70%";
	else if (larghezza>1400) charSize="70%";
	else if (larghezza>1300) charSize="70%";
	else if (larghezza>1200) charSize="70%";
	else if (larghezza>1100) charSize="70%";
	else if (larghezza>1000) charSize="70%";
	else if (larghezza>900) charSize="70%";
	else if (larghezza>800) charSize="70%";
	else if (larghezza>700) charSize="70%";
	else if (larghezza>600) charSize="70%";
	else charSize="70%";


	//vediamo quale è la maniglia trovata
	var arraySlideshow = new Array();
	for (i=0;i<oggetto.length;i++)
	{
		if (itemRilevato==oggetto[i].nome)
		{
			arraySlideshow=oggetto[i].elementi;
		}

	}

/*	if (itemRilevato== "FRAME")
	{
		var obj=new Object();
		obj.titolo="Guarnitura Q8 ros./bocc. quadra Patent ACL/PN";
		obj.img1="frame/FRAME_dettaglio_ARAB.png";
		obj.img2="frame/FRAME_dettaglio_misura_30x30.png";
		obj.text1="Cod. Prod.: 7747226160758<br>EAN: 8026105463122<br>Tipologia: maniglie rosette e bocchette<br>Codice tipologia: Q8 RBQ 30X30";
		obj.text2="Serie: FRAME<br>Finitura: Alluminio cromato lucido/pelle nera<br>Materiale: Alluminio<br>Disponibilità: consegna entro 5 settimane";
		arraySlideshow.push(obj);
		arraySlideshow.push(obj);
		arraySlideshow.push(obj);
	} 
*/
	var htmlToAdd="";

	// ne metto prima una che illustra lo slide
	htmlToAdd += "<section>";
	htmlToAdd += "<div style='background-color:white;text-align:center;width: 80%;border: 2px solid #b1c903;border-radius: 20px;position: absolute;right: 10%;top:50px;'>";
	htmlToAdd += "<image src='assets/slide.png' style='width:60%;margin-bottom: 0px;border: 0px;background: none;margin: 0px;margin-left:10%;box-shadow: none;' />";
	htmlToAdd += "<div style='clear:both;width:100%;display: inline-flex;'>";
	htmlToAdd += "<p class='testoMini' style='width:80%;text-align:center;color:#b1c903;margin-top:0%;padding-top: 2%;margin-bottom: 2%;margin-left:10%;border:0px solid green;font-size:"+charSize+"'>Scorri le slide per esaminare le schede dei prodotti</p>";
	htmlToAdd += "</div>";
	htmlToAdd += "</div>";
	htmlToAdd += "</section>";

	for (i=0;i<arraySlideshow.length;i++)
	{
		htmlToAdd += "<section>";
		htmlToAdd += "<div style='background-color:white;text-align:center;width: 80%;border: 2px solid #b1c903;border-radius: 20px;position: absolute;right: 10%;top:50px;'>";
		htmlToAdd += "<p class='menuTitle' style='text-align:center;color:#b1c903;margin-top:3%;margin-bottom: 0px;'>" + arraySlideshow[i].titolo + "</p>";
		htmlToAdd += "<image src='assets/" + arraySlideshow[i].img1 + "' style='width:50%;margin-bottom: 0px;float:left;border: 0px;background: none;margin: 0px;box-shadow: none;' /><image src='assets/" + arraySlideshow[i].img2 + "' style='width:50%;margin-bottom: 0px;float:left;border: 0px;background: none;margin: 0px;box-shadow: none;' />";
		htmlToAdd += "<div style='clear:both;width:100%;display: inline-flex;'>";
		htmlToAdd += "<p class='testoMini' style='float:left;width:48%;text-align:left;color:#b1c903;margin-top:0%;padding-top: 2%;margin-bottom: 2%;margin-left:2%;border:0px solid green;font-size:"+charSize+"'>" + arraySlideshow[i].text1 + "</p>";
		htmlToAdd += "<p class='testoMini' style='float:left;width:48%;text-align:left;color:#b1c903;margin-top:0%;padding-top: 2%;margin-bottom: 2%;margin-right:2%;border:0px solid green;font-size:"+charSize+"'>" + arraySlideshow[i].text2 + "</p>";
		htmlToAdd += "</div>";
		htmlToAdd += "</div>";
		htmlToAdd += "</section>";


	}
	console.log(htmlToAdd);

	$("#mySlides").html(htmlToAdd);
	$("#slideShow").show();
	

	$("#backButton").show();

	var altezzaHeader = $("#header").height();
	var larghezzaImg = $("#imgChiudi").width();
	$("#slideShow").css("top",altezzaHeader);
	$("#labelChiudi").css("font-size",altezzaHeader*0.6);
	$("#labelChiudi").css("margin-top",altezzaHeader*0.2);
	$("#labelChiudi").css("left",larghezzaImg+"px");




    Reveal.addEventListener( 'ready', function( event ) {
    	console.log("Reveal Ready");
    	Reveal.slide( 0 );
    	Reveal.configure({ transition: 'slide' });
	} );

    Reveal.initialize({
        margin: 0.01,
        //override width
        //height: 300,
        
        //override width
        //width: 1100,
        // Display controls in the bottom right corner
        controls: false,
        // Display a presentation progress bar
        progress: true,
        // Display the page number of the current slide
        slideNumber: false,
        // Push each slide change to the browser history
        history: false,
        // Enable keyboard shortcuts for navigation
        keyboard: false,
        // Enable the slide overview mode
        overview: true,
        // Vertical centering of slides
        center: true,
        // Enables touch navigation on devices with touch input
        touch: true,
        // Loop the presentation
        loop: false,
        // Change the presentation direction to be RTL
        rtl: false,
        // Turns fragments on and off globally
        fragments: true,
        // Flags if the presentation is running in an embedded mode,
        // i.e. contained within a limited portion of the screen
        embedded: false,
        // Flags if we should show a help overlay when the questionmark
        // key is pressed
        help: true,
        // Flags if speaker notes should be visible to all viewers
        showNotes: false,
        // Number of milliseconds between automatically proceeding to the
        // next slide, disabled when set to 0, this value can be overwritten
        // by using a data-autoslide attribute on your slides
        autoSlide: 0,
        // Stop auto-sliding after user input
        autoSlideStoppable: true,
        // Enable slide navigation via mouse wheel
        mouseWheel: false,
        // Hides the address bar on mobile devices
        hideAddressBar: true,
        // Opens links in an iframe preview overlay
        previewLinks: false,
        // Transition style
        transition: 'none', // none/fade/slide/convex/concave/zoom
        // Transition speed
        transitionSpeed: 'default', // default/fast/slow
        // Transition style for full page slide backgrounds
        backgroundTransition: 'none', // none/fade/slide/convex/concave/zoom
        // Number of slides away from the current that are visible
        viewDistance: 1,
        // Parallax background image
        parallaxBackgroundImage: '', // e.g. "'https://s3.amazonaws.com/hakim-static/reveal-js/reveal-parallax-1.jpg'"
        // Parallax background size
        parallaxBackgroundSize: '', // CSS syntax, e.g. "2100px 900px"
        // Amount to move parallax background (horizontal and vertical) on slide change
        // Number, e.g. 100
        parallaxBackgroundHorizontal: '',
        parallaxBackgroundVertical: ''

    });





}

function showVideo()
{
	abilitaOverlay(false);

	if (logEnabled==true) AR.logger.info("showVideo nome: "+ itemRilevato);
	console.log("sono in showVideo, con nome: "+itemRilevato);
    
 	//vediamo quale è la maniglia trovata
	var urlVideo = "";
	
	if (itemRilevato=="reginaElisabetta") urlVideo="reginaElisabetta.mp4";

	//quanto lo devo fare grande il video?
	var larghezzaDiv=$("#videoContainer").width();


	var htmlToAdd="";
	htmlToAdd += "<video autoplay controls width='"+larghezzaDiv*0.9+"' style='padding-top:"+larghezzaDiv*0.05+"px;padding-bottom:"+larghezzaDiv*0.05+"px;'>";
	//htmlToAdd += "<source src='/sdcard/Download/"+urlVideo+"' type='video/mp4' />";
	htmlToAdd += "<source src='assets/"+urlVideo+"' type='video/mp4' />";
	htmlToAdd += "</video>";

	console.log(htmlToAdd);

	$("#videoContainer").html(htmlToAdd);
	$("#videoContainer").show();

	$("#backButton").show();

	var altezzaHeader = $("#header").height();
	var larghezzaImg = $("#imgChiudi").width();
	//$("#slideShow").css("top",altezzaHeader);
	$("#labelChiudi").css("font-size",altezzaHeader*0.6);
	$("#labelChiudi").css("margin-top",altezzaHeader*0.2);
	$("#labelChiudi").css("left",larghezzaImg+"px");


}

function showContents()
{
	abilitaOverlay(false);

	if (logEnabled==true) AR.logger.info("showContents nome: "+ itemRilevato);
	console.log("sono in showContents, con nome: "+itemRilevato);
    
 	if (itemRilevato=="reginaElisabetta")
 	{
		var urlVideo = "";
		urlVideo="reginaElisabetta.mp4";

		//quanto lo devo fare grande il video?
		var larghezzaDiv=$("#videoContainer").width();


		var htmlToAdd="";
		htmlToAdd += "<video autoplay controls width='"+larghezzaDiv*0.9+"' style='padding-top:"+larghezzaDiv*0.05+"px;padding-bottom:"+larghezzaDiv*0.05+"px;'>";
		//htmlToAdd += "<source src='/sdcard/Download/"+urlVideo+"' type='video/mp4' />";
		htmlToAdd += "<source src='assets/"+urlVideo+"' type='video/mp4' />";
		htmlToAdd += "</video>";

		console.log(htmlToAdd);

		$("#videoContainer").html(htmlToAdd);
		$("#videoContainer").show();

		$("#backButton").show();

		var altezzaHeader = $("#header").height();
		var larghezzaImg = $("#imgChiudi").width();
		//$("#slideShow").css("top",altezzaHeader);
		$("#labelChiudi").css("font-size",altezzaHeader*0.6);
		$("#labelChiudi").css("margin-top",altezzaHeader*0.2);
		$("#labelChiudi").css("left",larghezzaImg+"px");

 	}
 	else if (itemRilevato=="marilyn")
	{

		//grandezza del carattere...
		var larghezza=$( window ).width();
		var altezza=$( window ).height();
		var altezzaImmagine=altezza*0.4;

		console.log( "larghezza:" + larghezza );
		console.log( "altezza:" + altezza );
		console.log( "altezzaImmagine:" + altezzaImmagine );
		var charSize="100%";

		if (larghezza>1600) charSize="60%";
		else if (larghezza>1500) charSize="60%";
		else if (larghezza>1400) charSize="60%";
		else if (larghezza>1300) charSize="60%";
		else if (larghezza>1200) charSize="60%";
		else if (larghezza>1100) charSize="60%";
		else if (larghezza>1000) charSize="60%";
		else if (larghezza>900) charSize="60%";
		else if (larghezza>800) charSize="60%";
		else if (larghezza>700) charSize="60%";
		else if (larghezza>600) charSize="60%";
		else charSize="60%";


		var arrayImg=[];
		var arrayTitoli=[];

		arrayImg.push("marilyn1.jpg");
		arrayImg.push("marilyn2.jpg");
		arrayImg.push("marilyn3.jpg");
		//arrayImg.push("marilyn4.jpg");
		//arrayImg.push("marilyn5.jpg");

		arrayTitoli.push("Andy Warhol in front of two paintings from his Marilyn series.");
		arrayTitoli.push("Marilyn Monroe (silk-screen printing by Andy Warhol, 1967)");
		arrayTitoli.push("Acrylic on canvas - 1962");
		//arrayTitoli.push("");
		//arrayTitoli.push("");

		var htmlToAdd="";

		// ne metto prima una che illustra lo slide
		htmlToAdd += "<section>";
		htmlToAdd += "<div style='background-color:white;text-align:center;width: 80%;border: 2px solid #b1c903;border-radius: 20px;position: absolute;right: 10%;top:50px;'>";
		htmlToAdd += "<image src='assets/slide.png' style='width:60%;margin-bottom: 0px;border: 0px;background: none;margin: 0px;margin-left:10%;box-shadow: none;' />";
		htmlToAdd += "<div style='clear:both;width:100%;display: inline-flex;'>";
		htmlToAdd += "<p class='testoMini' style='width:80%;text-align:center;color:#b1c903;margin-top:0%;padding-top: 2%;margin-bottom: 2%;margin-left:10%;border:0px solid green;font-size:"+charSize+"'>Scorri le slide per esaminare le schede dei prodotti</p>";
		htmlToAdd += "</div>";
		htmlToAdd += "</div>";
		htmlToAdd += "</section>";

		for (i=0;i<arrayImg.length;i++)
		{
			htmlToAdd += "<section>";
			htmlToAdd += "<div style='background-color:white;text-align:center;width: 80%;border: 2px solid #b1c903;border-radius: 20px;position: absolute;right: 10%;top:50px;'>";
			htmlToAdd += "<p class='menuTitle' style='text-align:center;color:#b1c903;margin-top:3%;margin-bottom: 0px;font-size:80%;'>" + arrayTitoli[i] + "</p>";
			htmlToAdd += "<image src='assets/" + arrayImg[i] + "' style='height:"+altezzaImmagine+"px;margin-bottom: 0px;border: 0px;background: none;margin: 0px;box-shadow: none;margin-top: 2%;' />";
			htmlToAdd += "<div style='clear:both;width:100%;display: inline-flex;'>";
			htmlToAdd += "</div>";
			htmlToAdd += "</div>";
			htmlToAdd += "</section>";


		}
		console.log(htmlToAdd);

		$("#mySlides").html(htmlToAdd);
		$("#slideShow").show();
		

		$("#backButton").show();

		var altezzaHeader = $("#header").height();
		var larghezzaImg = $("#imgChiudi").width();
		$("#slideShow").css("top",altezzaHeader);
		$("#labelChiudi").css("font-size",altezzaHeader*0.6);
		$("#labelChiudi").css("margin-top",altezzaHeader*0.2);
		$("#labelChiudi").css("left",larghezzaImg+"px");




	    Reveal.addEventListener( 'ready', function( event ) {
	    	console.log("Reveal Ready");
	    	Reveal.slide( 0 );
	    	Reveal.configure({ transition: 'slide' });
		} );

	    Reveal.initialize({
	        margin: 0.01,
	        //override width
	        //height: 300,
	        
	        //override width
	        //width: 1100,
	        // Display controls in the bottom right corner
	        controls: false,
	        // Display a presentation progress bar
	        progress: true,
	        // Display the page number of the current slide
	        slideNumber: false,
	        // Push each slide change to the browser history
	        history: false,
	        // Enable keyboard shortcuts for navigation
	        keyboard: false,
	        // Enable the slide overview mode
	        overview: true,
	        // Vertical centering of slides
	        center: true,
	        // Enables touch navigation on devices with touch input
	        touch: true,
	        // Loop the presentation
	        loop: false,
	        // Change the presentation direction to be RTL
	        rtl: false,
	        // Turns fragments on and off globally
	        fragments: true,
	        // Flags if the presentation is running in an embedded mode,
	        // i.e. contained within a limited portion of the screen
	        embedded: false,
	        // Flags if we should show a help overlay when the questionmark
	        // key is pressed
	        help: true,
	        // Flags if speaker notes should be visible to all viewers
	        showNotes: false,
	        // Number of milliseconds between automatically proceeding to the
	        // next slide, disabled when set to 0, this value can be overwritten
	        // by using a data-autoslide attribute on your slides
	        autoSlide: 0,
	        // Stop auto-sliding after user input
	        autoSlideStoppable: true,
	        // Enable slide navigation via mouse wheel
	        mouseWheel: false,
	        // Hides the address bar on mobile devices
	        hideAddressBar: true,
	        // Opens links in an iframe preview overlay
	        previewLinks: false,
	        // Transition style
	        transition: 'none', // none/fade/slide/convex/concave/zoom
	        // Transition speed
	        transitionSpeed: 'default', // default/fast/slow
	        // Transition style for full page slide backgrounds
	        backgroundTransition: 'none', // none/fade/slide/convex/concave/zoom
	        // Number of slides away from the current that are visible
	        viewDistance: 1,
	        // Parallax background image
	        parallaxBackgroundImage: '', // e.g. "'https://s3.amazonaws.com/hakim-static/reveal-js/reveal-parallax-1.jpg'"
	        // Parallax background size
	        parallaxBackgroundSize: '', // CSS syntax, e.g. "2100px 900px"
	        // Amount to move parallax background (horizontal and vertical) on slide change
	        // Number, e.g. 100
	        parallaxBackgroundHorizontal: '',
	        parallaxBackgroundVertical: ''

	    });


	}



}



function abilitaOggetto(indice, timeout)
{
	
	//console.log("indice check: "+ interno);
	setTimeout(function() {
		console.log("abilito oggetto: "+ indice);
		console.log("il tipo è: "+ arrayOverlay[indice].type);
		arrayOverlay[indice].enabled=true;
		//che cosa è?
		if (arrayOverlay[indice].type=="video")
		{
			console.log("è un video");
			actualVideo=arrayOverlay[indice];
			if (arrayOverlay[indice].comportamento=="play")
			{
				console.log("lo devo mettere in play se non lo è già");
				if (videoIsPlaying == false)
				{
					arrayOverlay[indice].play();
					arrayOverlay[indice].playing = true;
					videoIsPlaying = true;
				}
			}
			else
			{
				console.log("lo disabilito, aspettado il play");
				arrayOverlay[indice].enabled=false;
			}

		}
		

	}, timeout);
}

function disabilitatutto()
{
	if (logEnabled==true) AR.logger.info("disabilita");
	console.log("sono in disabilitatutto");
    
	for (i=0;i<arrayOverlay.length;i++)
	{
		arrayOverlay[i].enabled=false;
	}
	

	if (itemRilevato=="copertina")
	{
		  $("#textContainer" ).animate({
            right: "-35%"
          }, 1000, function() {
            // Animation complete.
            $("#textContainer").hide()
          });
	}

	$("#rettangolo").show();
	//$("#linea").show();
	//$("#griglia").show();


}


function loadDownloadedJson(myUrl)
{


	//devo cancellare tutto
	var lunghezza1=arrayOverlayGlobale.length;
	console.log("arrayOverlayGlobale.length: " + lunghezza1);
	for (var i=0;i<lunghezza1;i++)
	{
		var lunghezza2=arrayOverlayGlobale[i].length;
		console.log("arrayOverlay.length: " + lunghezza2);
		for (var t=0;t<lunghezza2;t++)
		{
			arrayOverlayGlobale[i][t].destroy();
		}
	}	


	lunghezza1=arrayTrackable.length;
	console.log("arrayTrackable.length: " + lunghezza1);
	for (var i=0;i<lunghezza1;i++)
	{
		arrayTrackable[i].destroy();
	}	


	oggetto = new Object();
	oggettoTemp = new Array();
	arrayOverlay = new Array();
	arrayOverlayGlobale = new Array(); //contiene n arrayOverlay, uno per ogni immagine da riconoscere nel tracker....
	arrayIndiciIndicatori = new Array(); //contiene gli indici degli indicatori principali nel'arrayOverlay
	arrayIndiciIndicatoriGlobale = new Array(); //contiene n arrayIndiciIndicatori, uno per ogni immagine da riconoscere nel tracker....
	indiceIndicatoreCorrente = -1;
	arrayCorrispondenzeNomi = new Array(); //contiene n nomi, che mi indicano a quale oggetto dell'array globale mi riferisco
	arrayTrackable = new Array();



	if (logEnabled==true) AR.logger.info("url: " + myUrl)

	$.ajax({
		url: myUrl,
		dataType: 'json'
		})
		.done(function( json )
		{

			console.log("caricato external json");
		    console.log(JSON.stringify(json));
		    for (i=0;i<json.oggetto.length;i++)
		    {
				var id=json.oggetto[i].id;
				//per ogni oggetto vedo gli elementi
			    var elementi=json.oggetto[i].elementi; 
			    for (t=0;t<elementi.length;t++)
			    {
					//per ogni oggetto vedo gli elementi
					var elemento=elementi[t]; 
					//ma per questo elemenato, ci sta almeno una risorsa per la fase selezionata?
					var almenoUnaRisorsa=false;

					var tempArray=new Array();
					tempArray.push(id);
					tempArray.push(elemento.id);
					//le risorse sono array esse stesse
					var risorse = new Array();
				    for (z=0;z<elemento.risorse.length;z++)
				    {
						//vediamo se lo posso prendere, dipendente dalla fase....
						if (elemento.risorse[z].risorsa_fase==fase)
						{
							almenoUnaRisorsa=true;
							var risorsa = new Array();
							if ((elemento.risorse[z].risorsa_tipo=="html") || (elemento.risorse[z].risorsa_tipo=="htm"))
							{
								risorsa.push("html");
								//estraggo il nome
								var uri = encodeURI(elemento.risorse[z].risorsa_percorso);
								var indice = uri.lastIndexOf("/");
								var name = uri.substring(indice+1);
								var prefisso="pr_"+i+""+t+""+z;
								//risorsa.push(prefisso+"/"+name);
								risorsa.push(prefisso+"/index.html");
								risorsa.push(elemento.risorse[z].risorsa_percorso);
								risorsa.push(elemento.risorse[z].risorsa_titolo);
							}
							else
							{	
								risorsa.push(elemento.risorse[z].risorsa_tipo);
								risorsa.push(elemento.risorse[z].risorsa_nome);
								risorsa.push(elemento.risorse[z].risorsa_percorso);
								risorsa.push(elemento.risorse[z].risorsa_titolo);
							}
							risorse.push(risorsa);
						}
					}
					tempArray.push(risorse);
					tempArray.push(elemento.visualizzato);
					//per quali livelli è valido questo elemento?
/*					var livelli = new Array();
				    for (z=0;z<elemento.livelli.length;z++)
				    {
						livelli.push(livelli[z]);
					}
*/					tempArray.push(elemento.livelli);

					if (almenoUnaRisorsa) oggettoTemp.push(tempArray);
				}
			}

			if (logEnabled==true) AR.logger.info("fatto - external json")
			if (logEnabled==true) AR.logger.info(JSON.stringify(oggettoTemp))
			console.log("fatto - external json");
			console.log(JSON.stringify(oggettoTemp));
			//carico il json interno e compongo l'oggetto finale come mix dei 2
			loadInternalJson();
		})
		.fail(function( jqXHR, textStatus ) {
	  		if (logEnabled==true) AR.logger.info("Request failed: " + textStatus)
	  		console.log( "Request failed: " + textStatus );
		}); 



}


function loadInternalJson()
{

	$.ajax({
		url: "assets/definizione.json",
		dataType: 'json'
		})
		.done(function( json )
		{

		    //carico anche quello appena scaricato, visto che l'oggetto finale sarà un mix dei 2
			console.log("Caricato JSON Interno");
			console.log(JSON.stringify(json));

		    //carico anche quello appena scaricato, visto che l'oggetto finale sarà un mix dei 2
			console.log("Caricato JSON Interno");
			console.log(JSON.stringify(json));

			var chiavi = Object.keys(json);

		    for (i=0;i<chiavi.length;i++)
		    {
				var obj=new Object();
				obj.nome=chiavi[i];
				obj.elementi=json[chiavi[i]].slideshow;
				obj.titoloVideo=json[chiavi[i]].titoloVideo;
				obj.urlVideo=json[chiavi[i]].urlVideo;
				oggetto.push(obj);
			}			

			console.log("fatto");
			console.log(JSON.stringify(oggetto));
			console.log("Faccio partire il tutto....");






			World.init();
		})
		.fail(function( jqXHR, textStatus ) {
	  		console.log( "Request failed: " + textStatus );
		}); 

	

}



function findObject(idToFind)
{
	//console.log("entro in findObject per cercare: "+idToFind);
	var ret = null;

	for (f=0;f<oggettoTemp.length;f++)
	{
		if (oggettoTemp[f][1]==idToFind)
		{
			ret = oggettoTemp[f];
		}
	}
	//console.log("Trovato: "+ JSON.stringify(ret));
	return ret;

}	


/*

//inizializzazione oggetti
var oggetto = new Object();
oggetto.tracker="tracker.wtc";
oggetto.immagini = new Array();

var immagine = new Object();
immagine.nome="caldaia1";

var elementi = new Array();

var elemento = new Object();
elemento.placeholder="caldaia1_1.png";
elemento.scale=0.375;
elemento.placeholderOffsetX=0.13;
elemento.placeholderOffsetY=0.19;
elementi.push(elemento);

elemento = new Object();
elemento.placeholder="caldaia1_1_indicatore.png";
elemento.scale=0.08;
elemento.placeholderOffsetX=0.34;
elemento.placeholderOffsetY=0.32;
elemento.interactionType="click";
elemento.action="video";
elemento.url="controlloFumi.mp4";
elementi.push(elemento);

elemento = new Object();
elemento.placeholder="caldaia1_2.png";
elemento.scale=0.108;
elemento.placeholderOffsetX=-0.16;
elemento.placeholderOffsetY=0.31;
elementi.push(elemento);

elemento = new Object();
elemento.placeholder="caldaia1_2_indicatore.png";
elemento.scale=0.0758;
elemento.placeholderOffsetX=-0.31;
elemento.placeholderOffsetY=0.34;
elemento.interactionType="click";
elemento.action="pdf";
elemento.url="pdfExample.pdf";
elementi.push(elemento);

immagine.elementi = elementi;

oggetto.immagini.push(immagine);
*/



var myOffsetX=0.20;
var myOffsetY=0.20;
var elementIndex=0;

function muovi(direzione)
{
	
	if (direzione=="sx") myOffsetX=myOffsetX-0.01;
	if (direzione=="dx") myOffsetX=myOffsetX+0.01;
	if (direzione=="up") myOffsetY=myOffsetY+0.01;
	if (direzione=="dw") myOffsetY=myOffsetY-0.01;
	if (direzione=="sx+") myOffsetX=myOffsetX-0.1;
	if (direzione=="dx+") myOffsetX=myOffsetX+0.1;
	if (direzione=="up+") myOffsetY=myOffsetY+0.1;
	if (direzione=="dw+") myOffsetY=myOffsetY-0.1;
	if (direzione=="next") elementIndex++;

	document.getElementById("offsetX").innerHTML="offsetx: "+myOffsetX.toFixed(2);
	document.getElementById("offsetY").innerHTML="offsety: "+myOffsetY.toFixed(2);

	//World.arrayOverlay[elementIndex].offsetX=myOffsetX;
	//World.arrayOverlay[elementIndex].offsetY=myOffsetY;
	arrayOverlay[elementIndex].offsetX=myOffsetX;
	arrayOverlay[elementIndex].offsetY=myOffsetY;



	//this.myOffsetX=this.myOffsetX+0.1;
	//this.overlayOne.offsetX=myOffsetX;
	//labelOffsetX.text = myOffsetX;
}


function prevImage(currentImage,numImages,imageBase,extension)
{
	currentImage--;
	if (currentImage==0) currentImage=numImages;
	$("#image").attr("src","assets/"+imageBase+"_"+currentImage+"."+extension);
	//riaggiorno gli onClick
	$("#frecciaSx").unbind('click');
	$("#frecciaSx").click(function(){ prevImage(currentImage,numImages,imageBase,extension); });
}

function nextImage(currentImage,numImages,imageBase,extension)
{
	currentImage++;
	if (currentImage>numImages) currentImage=1;
	$("#image").attr("src","assets/"+imageBase+"_"+currentImage+"."+extension);
	//riaggiorno gli onClick
	$("#frecciaDx").unbind('click');
	$("#frecciaDx").click(function(){ nextImage(currentImage,numImages,imageBase,extension); });
}

function closeImage()
{
	//$("#image").attr("src","");
	$("#imageContainer").hide();
	//$("#frecciaSx").hide();
	//$("#frecciaDx").hide();
	$("#chiudi").hide();
	//$("#frecciaSx").unbind('click');
	//$("#frecciaDx").unbind('click');

}


function back()
{
	abilitaOverlay(true);

	//ha premuto back... cosa ci stava sullo schermo? un video o lo slideshow?
	if ($("#slideShow").is(':visible'))
	{
		$("#slideShow").hide();
	}

	var htmlToAdd="";
	$("#videoContainer").html(htmlToAdd);
	$("#videoContainer").hide();



	
/*	//vedo se ci sono dei video visibili....
	for (var i=0;i<arrayOverlayGlobale.length;i++)
	{
		for (var t=0;t<arrayOverlayGlobale[i].length;t++)
		{
			if (arrayOverlayGlobale[i][t].zOrder==50) //è un video
			{
				console.log("Ho trovato un video a: " + i + " - " + t);
				if (arrayOverlayGlobale[i][t].enabled==true)
				{
					console.log("il video è visibile");
					//lo stoppo 
					//arrayOverlayGlobale[indexGlobale][indexVideo].enabled=false;
					arrayOverlayGlobale[i][t].pause();
					arrayOverlayGlobale[i][t].playing = false;
					arrayOverlayGlobale[i][t].enabled = false;

					//e rimetto i sottoindicatori che avevamo tolto
					if (indiceIndicatoreCorrente!=-1)
					{
						var indexCheck=indiceIndicatoreCorrente+1;
						while((indexCheck<arrayOverlayGlobale[i].length) && (arrayOverlayGlobale[i][indexCheck].zOrder!=10))
						{
							console.log("Processo l'indice: " + indexCheck);
							//console.log("sono dentro al while - zOrder: " + arrayOverlayGlobale[indexGlobale][indexCheck].zOrder);
							if (arrayOverlayGlobale[i][indexCheck].zOrder==20)
							{
								arrayOverlayGlobale[i][indexCheck].enabled=true;
							}
							indexCheck++;
						}

					}
				}
				else
				{
					console.log("il video non è visibile");
				}
			}

		}
	}
*/




	//$("#image").attr("src","");
	$("#imageContainer").hide();
	//$("#frecciaSx").hide();
	//$("#frecciaDx").hide();
	$("#backButton").hide();
	//$("#frecciaSx").unbind('click');
	//$("#frecciaDx").unbind('click');
}


function showFirstImage (imageBase,extension,numImages)
{
	console.log("Sono nello show prima immagine: "+imageBase);

	var htmlToAdd="";
    for (i=1;i<=numImages;i++)
    {
    	htmlToAdd += "<image src='assets/"+imageBase+"_"+i+"."+extension+"' style='width:100%;margin-bottom: 30px;'/>";
    }
	$("#imageContainer").html(htmlToAdd);
	$("#imageContainer").show();
	$("#chiudi").show();


/*
	$("#image").attr("src","assets/"+imageBase+"_1"+"."+extension);
	$("#imageContainer").show();
	$("#frecciaDx").click(function(){ nextImage(1,numImages,imageBase,extension); });
	$("#frecciaSx").click(function(){ prevImage(1,numImages,imageBase,extension); });
	$("#frecciaSx").show();
	$("#frecciaDx").show();
	$("#chiudi").show();
*/
}


function showImage (url1, url2)
{
	console.log("Sono nello show immagine: "+url1);

	var htmlToAdd="";
	htmlToAdd += "<section>";
	htmlToAdd += "<div style='background-color:white;text-align:center;width: 80%;border: 2px solid #b1c903;border-radius: 20px;position: absolute;right: 10%;top:50px;'>";
	htmlToAdd += "<p class='menuTitle' style='text-align:center;color:#b1c903;margin-top:3%;margin-bottom: 0px;'>Guarnitura Q8 ros./bocc. quadra Patent ACL/PN</p>";
	htmlToAdd += "<image src='assets/"+url1+"' style='width:50%;margin-bottom: 0px;float:left;border: 0px;background: none;margin: 0px;box-shadow: none;' /><image src='assets/"+url2+"' style='width:50%;margin-bottom: 0px;float:left;border: 0px;background: none;margin: 0px;box-shadow: none;' />";
	htmlToAdd += "<div style='clear:both;width:100%;display: inline-flex;'>";
	htmlToAdd += "<p class='menuText' style='float:left;width:48%;text-align:left;color:#b1c903;margin-top:0%;padding-top: 2%;margin-bottom: 2%;margin-left:2%;border:0px solid green;'>Cod. Prod.: 7747226160758<br>EAN: 8026105463122<br>Tipologia: maniglie rosette e bocchette<br>Codice tipologia: Q8 RBQ 30X30</p>";
	htmlToAdd += "<p class='menuText' style='float:left;width:48%;text-align:left;color:#b1c903;margin-top:0%;padding-top: 2%;margin-bottom: 2%;margin-right:2%;border:0px solid green;'>Serie: FRAME<br>Finitura: Alluminio cromato lucido/pelle nera<br>Materiale: Alluminio<br>Disponibilità: consegna entro 5 settimane</p>";
	htmlToAdd += "</div>";
	htmlToAdd += "</section>";
	htmlToAdd += "<section>";
	htmlToAdd += "<div style='background-color:white;text-align:center;width: 80%;border: 2px solid #b1c903;border-radius: 20px;position: absolute;right: 10%;top:50px;'>";
	htmlToAdd += "<p class='menuTitle' style='text-align:center;color:#b1c903;margin-top:3%;margin-bottom: 0px;'>Guarnitura Q8 ros./bocc. quadra Patent ACL/PN</p>";
	htmlToAdd += "<image src='assets/"+url1+"' style='width:50%;margin-bottom: 0px;float:left;border: 0px;background: none;margin: 0px;box-shadow: none;' /><image src='assets/"+url2+"' style='width:50%;margin-bottom: 0px;float:left;border: 0px;background: none;margin: 0px;box-shadow: none;' />";
	htmlToAdd += "<div style='clear:both;width:100%;display: inline-flex;'>";
	htmlToAdd += "<p class='menuText' style='float:left;width:48%;text-align:left;color:#b1c903;margin-top:0%;padding-top: 2%;margin-bottom: 2%;margin-left:2%;border:0px solid green;'>Cod. Prod.: 7747226160758<br>EAN: 8026105463122<br>Tipologia: maniglie rosette e bocchette<br>Codice tipologia: Q8 RBQ 30X30</p>";
	htmlToAdd += "<p class='menuText' style='float:left;width:48%;text-align:left;color:#b1c903;margin-top:0%;padding-top: 2%;margin-bottom: 2%;margin-right:2%;border:0px solid green;'>Serie: FRAME<br>Finitura: Alluminio cromato lucido/pelle nera<br>Materiale: Alluminio<br>Disponibilità: consegna entro 5 settimane</p>";
	htmlToAdd += "</div>";
	htmlToAdd += "</section>";
	$("#mySlides").html(htmlToAdd);
	$("#slideShow").show();
        Reveal.initialize({
            //override width
            width: 1100,
            // Display controls in the bottom right corner
            controls: false,
            // Display a presentation progress bar
            progress: true,
            // Display the page number of the current slide
            slideNumber: false,
            // Push each slide change to the browser history
            history: false,
            // Enable keyboard shortcuts for navigation
            keyboard: true,
            // Enable the slide overview mode
            overview: true,
            // Vertical centering of slides
            center: true,
            // Enables touch navigation on devices with touch input
            touch: true,
            // Loop the presentation
            loop: false,
            // Change the presentation direction to be RTL
            rtl: false,
            // Turns fragments on and off globally
            fragments: true,
            // Flags if the presentation is running in an embedded mode,
            // i.e. contained within a limited portion of the screen
            embedded: false,
            // Flags if we should show a help overlay when the questionmark
            // key is pressed
            help: true,
            // Flags if speaker notes should be visible to all viewers
            showNotes: false,
            // Number of milliseconds between automatically proceeding to the
            // next slide, disabled when set to 0, this value can be overwritten
            // by using a data-autoslide attribute on your slides
            autoSlide: 0,
            // Stop auto-sliding after user input
            autoSlideStoppable: true,
            // Enable slide navigation via mouse wheel
            mouseWheel: false,
            // Hides the address bar on mobile devices
            hideAddressBar: true,
            // Opens links in an iframe preview overlay
            previewLinks: false,
            // Transition style
            transition: 'default', // none/fade/slide/convex/concave/zoom
            // Transition speed
            transitionSpeed: 'default', // default/fast/slow
            // Transition style for full page slide backgrounds
            backgroundTransition: 'default', // none/fade/slide/convex/concave/zoom
            // Number of slides away from the current that are visible
            viewDistance: 3,
            // Parallax background image
            parallaxBackgroundImage: '', // e.g. "'https://s3.amazonaws.com/hakim-static/reveal-js/reveal-parallax-1.jpg'"
            // Parallax background size
            parallaxBackgroundSize: '', // CSS syntax, e.g. "2100px 900px"
            // Amount to move parallax background (horizontal and vertical) on slide change
            // Number, e.g. 100
            parallaxBackgroundHorizontal: '',
            parallaxBackgroundVertical: ''

        });
	//$("#backButton").show();
	//$("#chiudi").show();

}

function closeImage ()
{
	console.log("Sono nello close immagine: ");

	var htmlToAdd="";
	$("#imageContainer").html(htmlToAdd);
	$("#imageContainer").hide();

}


function showHtml (url)
{
	console.log("Sono nello show html: "+url);
	document.location = 'architectsdk://iframe.html?url='+url;


}


function sceltaFase (localFase)
{
	console.log("Ho scelto la fase: "+localFase);
	
	//var storage = window.sessionStorage;

	if (localFase==1)
	{
		fase="Inst";
		$("#faseLabel").html("Installazione");
		document.location = 'architectsdk://storage1';
	}
	else if (localFase==2)
	{
		fase="MiS"; 
		$("#faseLabel").html("Messa in Servizio");
		document.location = 'architectsdk://storage2';
	}
	else if (localFase==3)
	{
		fase="Man"; 
		$("#faseLabel").html("Manutenzione");
		document.location = 'architectsdk://storage3';
	}
	else
	{
		console.log("ERRORE, fase non scelta");
		return;
	}
	
	$("#sceltaFase").hide();
	loadDownloadedJson(parameter + "/definizione.json");

}

function size(grandezza)
{
	console.log("premuto " + grandezza);

	var lunghezza1=arrayOverlayGlobale.length;
	for (var i=0;i<lunghezza1;i++)
	{
		var lunghezza2=arrayOverlayGlobale[i].length;
		console.log("arrayOverlay.length: " + lunghezza2);
		for (var t=0;t<lunghezza2;t++)
		{
			//che cosa è e come mi devo comportare
			//console.log("oggetto: " + t + " - zorder: " + arrayOverlayGlobale[i][t].zOrder);	
			var zorder = arrayOverlayGlobale[i][t].zOrder;
			if (zorder==10) // il cerchio indicatore 
			{
				if (grandezza=="+") arrayOverlayGlobale[i][t].radius=arrayOverlayGlobale[i][t].radius+0.01;
				else arrayOverlayGlobale[i][t].radius=arrayOverlayGlobale[i][t].radius-0.01;
			}
			else if (zorder==15) //label del cerchio sottoindicatore
			{
				if (grandezza=="+")
				{
					arrayOverlayGlobale[i][t].height=arrayOverlayGlobale[i][t].height+0.01;
					arrayOverlayGlobale[i][t].offsetY=arrayOverlayGlobale[i][t].offsetY-0.01;
				}
				else
				{
					arrayOverlayGlobale[i][t].height=arrayOverlayGlobale[i][t].height-0.01;
					arrayOverlayGlobale[i][t].offsetY=arrayOverlayGlobale[i][t].offsetY+0.01;
				}
				//ci potrebbe essere una seconda label sotto... controllo
				if (arrayOverlayGlobale[i][t+1].zOrder==15)
				{
					t++;
					if (grandezza=="+")
					{
						arrayOverlayGlobale[i][t].height=arrayOverlayGlobale[i][t].height+0.01;
						arrayOverlayGlobale[i][t].offsetY=arrayOverlayGlobale[i][t].offsetY-0.02;
					}
					else
					{
						arrayOverlayGlobale[i][t].height=arrayOverlayGlobale[i][t].height-0.01;
						arrayOverlayGlobale[i][t].offsetY=arrayOverlayGlobale[i][t].offsetY+0.02;
					}
				}
			} 
		}
	}	




}

function toggleText()
{
    //comunque sia lo devo mettere davanti a tutto...

    //è aperto o chiuso?
    if ($("#textContainer").is(":visible"))
    {
        //alert("visibile");
       //$("#menuDiv").css("right","-40%");
        //$("#menuDiv").show();
        $("#textContainer" ).animate({
            right: "-35%"
          }, 500, function() {
            // Animation complete.
            $("#textContainer").hide()
          });

    }
    else
    {
        //alert("non visibile");
        $("#textContainer").css("right","-35%");
        $("#textContainer").show();
        $("#textContainer" ).animate({
            right: "5px"
          }, 500, function() {
            // Animation complete.
          });

    }
}



function testSlideshow()
{

		//grandezza del carattere...
		var larghezza=$( window ).width();
		var altezza=$( window ).height();
		var altezzaImmagine=altezza*0.4;

		console.log( "larghezza:" + larghezza );
		console.log( "altezza:" + altezza );
		console.log( "altezzaImmagine:" + altezzaImmagine );
		var charSize="100%";

		if (larghezza>1600) charSize="70%";
		else if (larghezza>1500) charSize="70%";
		else if (larghezza>1400) charSize="70%";
		else if (larghezza>1300) charSize="70%";
		else if (larghezza>1200) charSize="70%";
		else if (larghezza>1100) charSize="70%";
		else if (larghezza>1000) charSize="70%";
		else if (larghezza>900) charSize="70%";
		else if (larghezza>800) charSize="70%";
		else if (larghezza>700) charSize="70%";
		else if (larghezza>600) charSize="70%";
		else charSize="70%";


		var arrayImg=[];
		var arrayTitoli=[];

		arrayImg.push("marilyn1.jpg");
		arrayImg.push("marilyn2.jpg");
		arrayImg.push("marilyn3.jpg");
		//arrayImg.push("marilyn4.jpg");
		//arrayImg.push("marilyn5.jpg");

		arrayTitoli.push("Andy Warhol in front of two paintings from his Marilyn series.");
		arrayTitoli.push("Marilyn Monroe (Serigrafia di Andy Warhol, 1967)");
		arrayTitoli.push("Acrylic on canvas - 1962");
		//arrayTitoli.push("");
		//arrayTitoli.push("");

		var htmlToAdd="";

		// ne metto prima una che illustra lo slide
		htmlToAdd += "<section>";
		htmlToAdd += "<div style='background-color:white;text-align:center;width: 80%;border: 2px solid #b1c903;border-radius: 20px;position: absolute;right: 10%;top:50px;'>";
		htmlToAdd += "<image src='assets/slide.png' style='width:60%;margin-bottom: 0px;border: 0px;background: none;margin: 0px;margin-left:10%;box-shadow: none;' />";
		htmlToAdd += "<div style='clear:both;width:100%;display: inline-flex;'>";
		htmlToAdd += "<p class='testoMini' style='width:80%;text-align:center;color:#b1c903;margin-top:0%;padding-top: 2%;margin-bottom: 2%;margin-left:10%;border:0px solid green;font-size:"+charSize+"'>Scorri le slide per esaminare le schede dei prodotti</p>";
		htmlToAdd += "</div>";
		htmlToAdd += "</div>";
		htmlToAdd += "</section>";

		for (i=0;i<arrayImg.length;i++)
		{
			htmlToAdd += "<section>";
			htmlToAdd += "<div style='background-color:white;text-align:center;width: 80%;border: 2px solid #b1c903;border-radius: 20px;position: absolute;right: 10%;top:50px;'>";
			htmlToAdd += "<p class='menuTitle' style='text-align:center;color:#b1c903;margin-top:3%;margin-bottom: 0px;'>" + arrayTitoli[i] + "</p>";
			htmlToAdd += "<image src='assets/" + arrayImg[i] + "' style='height:"+altezzaImmagine+"px;margin-bottom: 0px;border: 0px;background: none;margin: 0px;box-shadow: none;margin-top: 2%;' />";
			htmlToAdd += "<div style='clear:both;width:100%;display: inline-flex;'>";
			htmlToAdd += "</div>";
			htmlToAdd += "</div>";
			htmlToAdd += "</section>";


		}
		console.log(htmlToAdd);

		$("#mySlides").html(htmlToAdd);
		$("#slideShow").show();
		

		$("#backButton").show();

		var altezzaHeader = $("#header").height();
		var larghezzaImg = $("#imgChiudi").width();
		$("#slideShow").css("top",altezzaHeader);
		$("#labelChiudi").css("font-size",altezzaHeader*0.6);
		$("#labelChiudi").css("margin-top",altezzaHeader*0.2);
		$("#labelChiudi").css("left",larghezzaImg+"px");




	    Reveal.addEventListener( 'ready', function( event ) {
	    	console.log("Reveal Ready");
	    	Reveal.slide( 0 );
	    	Reveal.configure({ transition: 'slide' });
		} );

	    Reveal.initialize({
	        margin: 0.01,
	        //override width
	        //height: 300,
	        
	        //override width
	        //width: 1100,
	        // Display controls in the bottom right corner
	        controls: false,
	        // Display a presentation progress bar
	        progress: true,
	        // Display the page number of the current slide
	        slideNumber: false,
	        // Push each slide change to the browser history
	        history: false,
	        // Enable keyboard shortcuts for navigation
	        keyboard: false,
	        // Enable the slide overview mode
	        overview: true,
	        // Vertical centering of slides
	        center: true,
	        // Enables touch navigation on devices with touch input
	        touch: true,
	        // Loop the presentation
	        loop: false,
	        // Change the presentation direction to be RTL
	        rtl: false,
	        // Turns fragments on and off globally
	        fragments: true,
	        // Flags if the presentation is running in an embedded mode,
	        // i.e. contained within a limited portion of the screen
	        embedded: false,
	        // Flags if we should show a help overlay when the questionmark
	        // key is pressed
	        help: true,
	        // Flags if speaker notes should be visible to all viewers
	        showNotes: false,
	        // Number of milliseconds between automatically proceeding to the
	        // next slide, disabled when set to 0, this value can be overwritten
	        // by using a data-autoslide attribute on your slides
	        autoSlide: 0,
	        // Stop auto-sliding after user input
	        autoSlideStoppable: true,
	        // Enable slide navigation via mouse wheel
	        mouseWheel: false,
	        // Hides the address bar on mobile devices
	        hideAddressBar: true,
	        // Opens links in an iframe preview overlay
	        previewLinks: false,
	        // Transition style
	        transition: 'none', // none/fade/slide/convex/concave/zoom
	        // Transition speed
	        transitionSpeed: 'default', // default/fast/slow
	        // Transition style for full page slide backgrounds
	        backgroundTransition: 'none', // none/fade/slide/convex/concave/zoom
	        // Number of slides away from the current that are visible
	        viewDistance: 1,
	        // Parallax background image
	        parallaxBackgroundImage: '', // e.g. "'https://s3.amazonaws.com/hakim-static/reveal-js/reveal-parallax-1.jpg'"
	        // Parallax background size
	        parallaxBackgroundSize: '', // CSS syntax, e.g. "2100px 900px"
	        // Amount to move parallax background (horizontal and vertical) on slide change
	        // Number, e.g. 100
	        parallaxBackgroundHorizontal: '',
	        parallaxBackgroundVertical: ''

	    });

}

function appareRettangolo()
{

	$( "#rettangolo" ).animate({
		    opacity: 1.0
		  }, 1000, scompareRettangolo)

}

function scompareRettangolo()
{

	$( "#rettangolo" ).animate({
		    opacity: 0.0
		  }, 1000, appareRettangolo)

}

function muoviLineaDx()
{

	$( "#linea" ).animate({
		    left: "90%"
		  }, 2000, muoviLineaSx)

}

function muoviLineaSx()
{

	$( "#linea" ).animate({
		    left: "10%"
		  }, 2000, muoviLineaDx)

}

$(document).ready(function()
{
	//testSlideshow();
	scompareRettangolo();
	//muoviLineaDx();
});
	
