cordova.define("com.wikitude.phonegap.WikitudePlugin.WikitudePlugin", function(require, exports, module) { 	
	/**
	 * Release date: 09.03.15
	 */

	var WikitudePlugin = function() {

		/**
		 *  This is the SDK Key, provided to you after you purchased the Wikitude SDK from http =//www.wikitude.com/store/.
		 *  You can obtain a free trial key at http =//www.wikitude.com/developer/licenses .
		 */
		this._sdkKey = "azTfqoscPuUDiNoedS6o9CIsOSf2wOrPChKBqxWNOlBLgaH10kxbcMeo3T+X2x1oW3zJMTT1hmPbX0jBZFKVMIIf4aD3i8+SwAmbwXa/xy5CwoR6UYZk+EQ38heXLHIeMdLy9mmb63Zgn5klZi6y1rQRj6aKKS9WxEAV3WHOjVtTYWx0ZWRfXyRlH29pbn500MKvm2glUsGwQySSDW8IhBt7cY7ERYjGj1ySnJnVaOOL4uM03aegDTrI4ol9XY5KeKy9kqNnrwsBQKBPJfVmqzicZ8UP9556sog9GrAJ7nDKAI4V1AJwFEKsBMGqRM8NvjgB5KgNEKUcCA7W6i5WsTdwiuvagPMqsNaXUnaueR3UTZD2FOnyiC/8J0klHqsR/L1B+Bo7Med9C64Go2gPAL+pUxFpdE6y+KadibpwGeuTmrh5vNDZcisTb938+E+jx5QQj3P3NOkEXmCNnRCBZcfNyGcxwTl0FQQc0CX64D4iTlL/n5SETDTlDBe5JPDhcMY9IJTyMG70IKnBt//GBV6MtC6KBwNC0sTJt6zIeHtrpVWbZaoxjAafCsxsHLqOhrLi5l0nP9BigVYKsW4k/qEwCjzu6rl0GNep+jL3rLCQlY059yPjI80l4AUn7G1/e3Fj+4c6HmUwrKDDoeZEJilS5hCnvLoBpIQb+Iz/BxkOl8uoYItaZMW95z2fpUvSBVtGM4GMpgKX6RLssJPHaBzB/VAeeBJlFJQDfNLqVQk=";

		//this._sdkKey = "y1MlI5aM8cOpnfnPWUhE6cJlGt3SNtcDxsWE/1WVPjU161qG1tdDepqeE+O/8YB7omiRkZujCoVOq/AwDUS7LjBWeDx1Xr/vOGyPs2JtcDYMkHHsQmOz0i86sy7hIP4+CwmwBmGXof5V8mNzSIZf/dm196CPP6ISTRgr9j8Pyi1TYWx0ZWRfX9ntuOMLDH7cghjqIIG6LhME9yftpAupOU9tY9y6GXT1kkbO5DBUANACrVju1/m7ngBWU3b0uIo30Fy34l/T6S0WID3L2lTgnmzFSM2PaVs8mHzV41b3bpfULZYdVH7z3krYASZsHCx9vI6Viiel+zDxdyoOdSJdexnSIbtVsGY+9B8KVgX9xMGGktHBjXR50m1TDNztLVPdjPIvFICJ9Ygay44I+iYHaPF/fpSD3jPitgql3UORxZOcnKlTitq1DUg8RiNv5Rxqb9/usjl7CbMFe+f69pt1llNRrnlv0E72mWotILGjc6DGCmXMSH3SVVtX2W0tp0Up3JxUU8TCpY2tZEaqMmFsSGfJqwiyLLhcManE7U5yUCHHou5dTyqqe4vJt3wsADV8lyVUuXZ8UxdRr6ielOIn5FYEaZT219qv/FUWoQM2JoNsec9c99k6nK5FMMLL1Hx1FTjztHjXv48p/ivuCV4+EYaxxCLwjzOXY7ZSDoFCzVlkkZD/ixB2u61+KaruTal5";
		//this._sdkKey = "Qzjjbp5JvRJdE4ADONkvQMR1vtdIePhMo122KI9qedsvOvXla4ENDSIUyGFUENU9gJdPyUMnB5GDS04mVFtH9oxPZbiYbFwHb3pr9GPBypIW1jH+1mgyeqBif5S+7fDxQ6Zc5LEeXDsxU5+w+9a4oSImAZeJyhyUtp9qxxIgURtTYWx0ZWRfX9OKVbfBvwDuWqica1C/tGP8DaxOPF7gq/Un4Si713zK5Nn9Nx2ON6jgTthNvXQrEJvFSF4VuDx0H+7NWIVj+mOHfcEMD6wyXrf9Zv5hkmnhyXICa38wUM7o+Xau+Dx5x2Nt3WgQ1ScQiDx6yJp9xh9APR0JpLUN0KVTqrVPLiTADjzisTrwWnU7XU3valM20GQaLKioGL6rJ1oFsuaRhdEWIi3JKdGpAq7+0Euz71183Le/5FhqEDwg4gc/KNMn6QeOcLNiXmqZvY6F1wXWWCma/NCuiloRnS7HJvv6TpNuYLpMFfon0vg/DZJIlEwJFQPSpT/Ofp7hgHV44yPQRJDcBfOCHcpHS6KTy0ERptjjMpRSYlmQQ3FhTvO85kWhbB1I3pKQX5F+h83fmlFTDUB8rbOlsaricadv6ZVHaGKv0dsKoWo1+rQDgLoJevUPqhAO8akvmCmZe2USVJx4wh/HECNSw+ntFrmAVHgEaHYLVDMhra42d5s=";

		/**
		 *  The Wikitude SDK can run in different modes.
		 *      * Geo means, that objects are placed at latitude/longitude positions.
		 *      * 2DTracking means that only image recognition is used in the ARchitect World.
		 *  When your ARchitect World uses both, geo and ir, than set this value to "IrAndGeo". Otherwise, if the ARchitectWorld only needs image recognition, placing "IR" will require less features from the device and therefore, support a wider range of devices. Keep in mind that image recognition requires a dual core cpu to work satisfyingly.
		 */
        this.FeatureGeo         = "geo";
        this.Feature2DTracking  = "2d_tracking";

        /**
         *  Start-up configuration: camera position (front or back).
         */
        this.CameraPositionUndefined = 0;
        this.CameraPositionFront     = 1;
        this.CameraPositionBack      = 2;
                       
        /**
         *  Start-up configuration: camera focus range restriction (for iOS only).
         */
        this.CameraFocusRangeNone = 0;
        this.CameraFocusRangeNear = 1;
        this.CameraFocusRangeFar  = 2;
        
	};


	/*
	 *	=============================================================================================================================
	 *
	 *	PUBLIC API
	 *
	 *	=============================================================================================================================
	 */

	/* Managing ARchitect world loading */

	/**
	 *  Use this function to check if the current device is capable of running ARchitect Worlds.
	 *
	 * @param {function} successCallback A callback which is called if the device is capable of running ARchitect Worlds.
	 * @param {function} errorCallback A callback which is called if the device is not capable of running ARchitect Worlds.
	 */
	WikitudePlugin.prototype.isDeviceSupported = function(successCallback, errorCallback, requiredFeatures) {

		// Check if the current device is capable of running Architect Worlds
		cordova.exec(successCallback, errorCallback, "WikitudePlugin", "isDeviceSupported", [requiredFeatures]);
	};

	/**
	 *	Use this function to load an ARchitect World.
	 *
     *  @param {function(loadedURL)}  		successCallback		function which is called after a successful launch of the AR world.
     *  @param {function(error)}		 	errorCallback		function which is called after a failed launch of the AR world.
     *	@param {String} 					architectWorldPath	The path to a local ARchitect world or to a ARchitect world on a server or your 
	 *  @param {String} 					worldPath			path to an ARchitect world, either on the device or on e.g. your Dropbox.
     *  @param {Array} 						requiredFeatures	augmented reality features: a flags mask for enabling/disabling 
     *                                  geographic location-based (WikitudePlugin.FeatureGeo) or image recognition-based (WikitudePlugin.Feature2DTracking) tracking.
	 *  @param {json object} (optional) startupConfiguration	represents the start-up configuration which may look like the following:
	 *									{
	 *                               		"cameraPosition": app.WikitudePlugin.CameraPositionBack,
	 *                                  	    	"*OptionalPlatform*": {
	 *											"*optionalPlatformKey*": "*optionalPlatformValue*"
	 *                                      	}
	 *                               	}
	 */	 
	WikitudePlugin.prototype.loadARchitectWorld = function(successCallback, errorCallback, architectWorldPath, requiredFeatures, startupConfiguration) {
        
		cordova.exec(successCallback, errorCallback, "WikitudePlugin", "open", [{
				"SDKKey": this._sdkKey,
				"ARchitectWorldURL": architectWorldPath,
				"RequiredFeatures": requiredFeatures,
		    	"StartupConfiguration" : startupConfiguration
			}]);
		
		// We add an event listener on the resume and pause event of the application life-cycle
		document.addEventListener("resume", this.onResume, false);
		document.addEventListener("pause", this.onPause, false);
		document.addEventListener("backbutton", this.onBackButton, false);
	};

	/* Managing the Wikitude SDK Lifecycle */
	/**
	 *	Use this function to stop the Wikitude SDK and to remove it from the screen.
	 */
	WikitudePlugin.prototype.close = function() {

		document.removeEventListener("pause", this.onPause, false);
		document.removeEventListener("resume", this.onResume, false);
		document.removeEventListener("backbutton", this.onBackButton, false);

		cordova.exec(this.onWikitudeOK, this.onWikitudeError, "WikitudePlugin", "close", [""]);
	};

	/**
	 *	Use this function to only hide the Wikitude SDK. All location and rendering updates are still active.
	 */
	WikitudePlugin.prototype.hide = function() {
		cordova.exec(this.onWikitudeOK, this.onWikitudeError, "WikitudePlugin", "hide", [""]);
	};

	/**
	 *	Use this function to show the Wikitude SDK again if it was hidden before.
	 */
	WikitudePlugin.prototype.show = function() {
		cordova.exec(this.onWikitudeOK, this.onWikitudeError, "WikitudePlugin", "show", [""]);
	};

	/* Interacting with the Wikitude SDK */

	/**
	 *	Use this function to call JavaScript which will be executed in the context of the currently loaded ARchitect World.
	 *
	 * @param js The JavaScript that should be evaluated in the ARchitect View.
	 */
	WikitudePlugin.prototype.callJavaScript = function(js) {
		cordova.exec(this.onWikitudeOK, this.onWikitudeError, "WikitudePlugin", "callJavascript", [js]);
	};

	/**
	 *	Use this function to set a callback which will be invoked when the ARchitect World opens an architectsdk =// url.
	 *	document.location = "architectsdk =//opendetailpage?id=9";
	 *
	 *	@param onUrlInvokeCallback A function which will be called when the ARchitect World invokes a call to "document.location = architectsdk =//"
	 */
	WikitudePlugin.prototype.setOnUrlInvokeCallback = function(onUrlInvokeCallback) {
		cordova.exec(onUrlInvokeCallback, this.onWikitudeError, "WikitudePlugin", "onUrlInvoke", [""]);
	};

	/**
	 *  Use this function to inject a location into the Wikitude SDK.
	 *
	 *  @param latitude The latitude which should be simulated
	 *  @param longitude The longitude which should be simulated
	 *  @param altitude The altitude which should be simulated
	 *  @param accuracy The simulated location accuracy
	 */
	WikitudePlugin.prototype.setLocation = function(latitude, longitude, altitude, accuracy) {
		cordova.exec(this.onWikitudeOK, this.onWikitudeError, "WikitudePlugin", "setLocation", [latitude, longitude, altitude, accuracy]);
	};

	/**
	 *  Use this function to generate a screenshot from the current Wikitude SDK view.
	 *
     *  @param {function(ur)}  successCallback  function which is called after the screen capturing succeeded.
     *  @param {function(err)} errorCallback    function which is called after capturing the screen has failed.
	 *  @param includeWebView Indicates if the ARchitect web view should be included in the generated screenshot or not.
	 *  @param imagePathInBundleorNullForPhotoLibrary If a file path or file name is given, the generated screenshot will be saved in the application bundle. Passing null will save the photo in the device photo library.
	 */
	WikitudePlugin.prototype.captureScreen = function(successCallback, errorCallback, includeWebView, imagePathInBundleOrNullForPhotoLibrary)
    {
		cordova.exec(successCallback, errorCallback, "WikitudePlugin", "captureScreen", [includeWebView, imagePathInBundleOrNullForPhotoLibrary]);
	};
	
	/**
	 * Use this function to set a callback that is called every time the Wikitude SDK encounters an internal error or warning. 
	 * Internal errors can occur at any time and might not be related to any WikitudePlugin function invocation.
	 * An error code and message are passed in form of a JSON object.
	 *
	 *  @param {function(jsonObject)}  errorHandler  function which is called every time the SDK encounters an internal error.
	 *
	 * NOTE: The errorHandler is currently only called by the Wikitude iOS SDK!
	 */
	WikitudePlugin.prototype.setErrorHandler = function(errorHandler)
	{
		cordova.exec(this.onWikitudeOK, errorHandler, "WikitudePlugin", "setErrorHandler", []);
	}

	/*
	 *	=============================================================================================================================
	 *
	 *	Callbacks of public functions
	 *
	 *	=============================================================================================================================
	 */


	/* Lifecycle updates */
	/**
	 *	This function gets called every time the application did become active.
	 */
	WikitudePlugin.prototype.onResume = function() {

		// Call the Wikitude SDK that it should resume.
		cordova.exec(this.onWikitudeOK, this.onWikitudeError, "WikitudePlugin", "onResume", [""]);
	};

	/* Lifecycle updates */
	/**
	 *	This function gets called every time the application did become active.
	 */
	WikitudePlugin.prototype.onBackButton = function() {

		// Call the Wikitude SDK that it should resume.
		//cordova.exec(this.onWikitudeOK, this.onWikitudeError, "WikitudePlugin", "close", [""]);
		WikitudePlugin.prototype.close();
	};

	/**
	 *	This function gets called every time the application is about to become inactive.
	 */
	WikitudePlugin.prototype.onPause = function() {

		// Call the Wikitude SDK that the application did become inactive
		cordova.exec(this.onWikitudeOK, this.onWikitudeError, "WikitudePlugin", "onPause", [""]);
	};

	/**
	 *	A generic success callback used inside this wrapper.
	 */
	WikitudePlugin.prototype.onWikitudeOK = function() {};

	/**
	 *  A generic error callback used inside this wrapper.
	 */
	WikitudePlugin.prototype.onWikitudeError = function() {};



	/* Export a new WikitudePlugin instance */
	var wikitudePlugin = new WikitudePlugin();
	module.exports = wikitudePlugin;
});
